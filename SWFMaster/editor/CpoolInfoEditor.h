#ifndef CPOOLINFOEDITOR_H
#define CPOOLINFOEDITOR_H

#include <QWidget>

namespace Ui {
class CpoolInfoEditor;
}

class CpoolInfoEditor : public QWidget
{
    Q_OBJECT

public:
    explicit CpoolInfoEditor(QWidget *parent = 0);
    ~CpoolInfoEditor();

private:
    Ui::CpoolInfoEditor *ui;
};

#endif // CPOOLINFOEDITOR_H
