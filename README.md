SWFMaster
=========

SWF字节码编辑器，未完成，正在开发中。可以用来：
* 查看和修改DoABC中的byte code
* 替换SWF中的资源
* 导出SWF中的资源
* 反编译
* SWF代码混淆
* ...

目前进度
========
![Screenshot 4][4]
![Screenshot 1][1]
![Screenshot 2][2]
![Screenshot 3][3]

[1]: SWFMaster/raw/master/Screenshot/1.jpg
[2]: SWFMaster/raw/master/Screenshot/2.jpg
[3]: SWFMaster/raw/master/Screenshot/3.jpg
[4]: SWFMaster/raw/master/Screenshot/4.jpg