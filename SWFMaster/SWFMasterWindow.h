#ifndef SWFMASTERWINDOW_H
#define SWFMASTERWINDOW_H

#include <QMainWindow>

class SWF;
class SWFTreeModel;
class EditorContainer;

namespace Ui {
class SWFMasterWindow;
}

class SWFMasterWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SWFMasterWindow(QWidget *parent = 0);
    ~SWFMasterWindow();
private slots:
	void onOpen();
	void onAbout();
	void onSaveAs();
	void onItemTreeContextMenuRequested(const QPoint& pos);
	void onItemTreeSelectItem(const QModelIndex& index);
private:
	Ui::SWFMasterWindow *ui;
	SWF* _swf;
    SWFTreeModel* _swfObjectModel;
	EditorContainer* _editorContainer;
};

#endif // SWFMASTERWINDOW_H
