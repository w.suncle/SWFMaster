#include "Editor.h"

DefaultHexEditor::DefaultHexEditor( QWidget* parent )
	: QHexEdit(parent)
{

}

const QString DefaultHexEditor::getEditorName() const
{
	return "Hex";
}

QWidget* DefaultHexEditor::asWidget() const
{
	return (QWidget*) this;
}

