#ifndef SWFTag_h__
#define SWFTag_h__

#include "SWFTypes.h"
#include "SWFStructs.h"
#include <string>
#include <vector>
#include <map>
#include "ByteArray.h"
#include "SWFInputStream.h"
#include "SWFOutputStream.h"

class TagHandler;
class DefineTag;

#define HAS_CLIP_ACTION  1 << 7
#define HAS_CLIP_DEPTH  1 << 6
#define HAS_NAME  1 << 5
#define HAS_RATIO  1 << 4
#define HAS_CXFORM  1 << 3
#define HAS_MATRIX  1 << 2
#define HAS_CHARACTER  1 << 1
#define HAS_MOVE  1 << 0

#define HAS_IMAGE  1 << 4
#define HAS_CLASS_NAME  1 << 3
#define HAS_CACHE_AS_BITMAP  1 << 2
#define HAS_BLEND_MODE  1 << 1
#define HAS_FILTER_LIST  1 << 0

class SWFTag
{
public:
	int code;
	size_t length;
	char* data;
protected:
	std::string _tagName;
	std::vector<SWFTag*> _references;
public:
	SWFTag();
	SWFTag(int tagCode) : code(tagCode) {}
	~SWFTag();
	void putReference(SWFTag* tag);
	void removeReference(SWFTag* tag);
	virtual void setData(char* data, size_t len);
	virtual void read(SWFInputStream* stream);
	virtual void getReferences(std::vector<SWFTag*>&);
	virtual void write(SWFOutputStream* stream);
	virtual void visit(TagHandler*);
	virtual std::string getTagName() const;
};

class DefineTag
	: public SWFTag
{
public:
	UI16 characterID;
	ByteArray characterData;
};

#pragma region Control Tags

class TagMetadata : public SWFTag
{
public:
	std::string text;
public:
	void setData(char* data, size_t len) override;
	void visit(TagHandler*) override;
};

class TagProductInfo
	: public SWFTag
{
public:
	void visit(TagHandler* h);
};

class TagSetBackgroundColor
	: public SWFTag
{
public:
	ColorRGB color;
public:
	TagSetBackgroundColor();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagFrameLabel
	: public SWFTag
{
public:
	std::string name;
public:
	TagFrameLabel();
	void visit(TagHandler* h) override;
};

class TagFileAttributes
	: public SWFTag
{
public:
	bool UseDirectBlit;
	bool UseGPU;
	bool HasMetadata;
	bool ActionScript3;
	bool UseNetwork;

public:
	TagFileAttributes();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagEnd
	: public SWFTag
{
public:
	TagEnd();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagScriptLimits
	: public SWFTag
{
public:
	TagScriptLimits();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagSymbolClass
	: public SWFTag
{
public:
	std::string topLevelClass;
	std::map<std::string, SWFTag*> tags;
public:
	TagSymbolClass();
	void visit(TagHandler* h) override;
};

class TagImportAssets2
	: public SWFTag
{
public:
	TagImportAssets2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagProtect
	: public SWFTag
{
public:
	TagProtect();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagExportAssets
	: public SWFTag
{
public:
	TagExportAssets();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagEnableDebugger
	: public SWFTag
{
public:
	TagEnableDebugger();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagEnableDebugger2
	: public SWFTag
{
public:
	TagEnableDebugger2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagSetTabIndex
	: public SWFTag
{
public:
	TagSetTabIndex();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineScalingGrid
	: public DefineTag
{
public:
	Rect Splitter;
public:
	TagDefineScalingGrid();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineSceneAndFrameLabelData
	: public DefineTag
{
public:
	TagDefineSceneAndFrameLabelData();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Control Tags

#pragma region Shapes
class TagDefineShape
	: public DefineTag
{
public:
	Rect ShapeBounds;
	ShapeWithStyle* Shapes;
public:
	TagDefineShape();
	void visit(TagHandler* h) override;
};

class TagDefineShape2
	: public TagDefineShape
{
public:
	TagDefineShape2();
	void visit(TagHandler* h) override;
};

class TagDefineShape3
	: public TagDefineShape
{
public:
	TagDefineShape3();
	void visit(TagHandler* h) override;
};

class TagDefineShape4
	: public TagDefineShape
{
public:
	Rect EdgeBounds;
	// UB[1]  If 1, use fill winding rule. Minimum
	// file format version is SWF 10
	UB UsesFillWindingRule;
	// UB[1]  If 1, the shape contains at least one
	// non-scaling stroke.
	UB UsesNonScalingStrokes;
	// UB[1]  If 1, the shape contains at least one
	// scaling stroke.
	UB UsesScalingStrokes;
public:
	TagDefineShape4();
	void visit(TagHandler* h) override;
};
#pragma endregion Shapes

#pragma region Sound
class TagDefineSound
	: public DefineTag
{
public:
	ByteArray soundData;
public:
	void visit(TagHandler* h) override;
};
#pragma endregion Sound

#pragma region Buttons

class TagDefineButton
	: public DefineTag
{
public:
	std::vector<ButtonRecord*> Characters;
	std::vector<ActionRecord*> Actions;
public:
	TagDefineButton();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

class TagDefineButton2
	: public DefineTag
{
public:
	UB TrackAsMenu;
	UI16 ActionOffset;
	std::vector<ButtonRecord*> Characters;
public:
	TagDefineButton2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

class TagDefineButtonCxform
	: public DefineTag
{
public:
	// Button ID for this information
	UI16 ButtonId;
	// Character color transform
	CXFORM ButtonColorTransform;
public:
	TagDefineButtonCxform();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

class TagDefineButtonSound
	: public DefineTag
{
public:
	TagDefineButtonSound();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

#pragma endregion Buttons

#pragma region Sprites And Movie Clips
class TagDefineSprite
	: public DefineTag
{
public:
	UI16 frameCount;
	std::vector<SWFTag*> tags;
public:
	TagDefineSprite();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Sprites And Movie Clips

#pragma region Shape Morphing
class TagDefineMorphShape
	: public DefineTag
{
public:
	TagDefineMorphShape();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineMorphShape2
	: public DefineTag
{
public:
	TagDefineMorphShape2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Shape Morphing

#pragma region Fonts and Text
class TagDefineFont
	: public DefineTag
{
public:
	TagDefineFont();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFont2
	: public DefineTag
{
public:
	TagDefineFont2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFont3
	: public DefineTag
{
public:
	// UB[1]
	// Has font metrics/layout information.
	UB FontFlagsHasLayout;
	// UB[1]
	// ShiftJIS encoding.
	UB FontFlagsShiftJIS;
	// UB[1]
	// SWF 7 or later: Font is small. Character glyphs
	// are aligned on pixel boundaries for dynamic and input text.
	UB FontFlagsSmallText;
	// UB[1]
	// ANSI encoding.
	UB FontFlagsANSI;
	// UB[1]
	// If 1, uses 32 bit offsets.
	UB FontFlagsWideOffsets;
	// UB[1]
	// Must be 1.
	UB FontFlagsWideCodes;
	// UB[1]
	// Italic Font
	UB FontFlagsItalic;
	// UB[1]
	// Bold Font.
	UB FontFlagsBold;
	// SWF 5 or earlier: always 0; SWF 6 or later: language code(UI8)
	UI8 LanguageCode;
	// UI8
	// Length of name.
	UI8 FontNameLen;
	// UI8[FontNameLen]
	// Name of font (see DefineFontInfo).
	std::string FontName;
	// UI16
	// Count of glyphs in font. May be zero for device fonts.
	UI16 NumGlyphs;
	// If FontFlagsWideOffsets, UI32[NumGlyphs] Otherwise UI16[NumGlyphs]
	// Array of shape offsets
	std::vector<UI32> OffsetTable;
	// If FontFlagsWideOffsets, UI32 Otherwise UI16
	// Byte count from start of OffsetTable to start of CodeTable.
	UI32 CodeTableOffset;
	// Array of shapes
	std::vector<Shape*> GlyphShapeTable;
	// UI16[NumGlyphs]
	// Sorted in ascending order. Always UCS-2 in SWF 6 or later.
	std::vector<UI16> CodeTable;
	// If FontFlagsHasLayout, UI16
	// Font ascender height.
	UI16 FontAscent;
	// If FontFlagsHasLayout, UI16
	// Font descender height.
	UI16 FontDescent;
	// If FontFlagsHasLayout, SI16
	// Font leading height (see following).
	SI16 FontLeading;
	// If FontFlagsHasLayout, SI16[NumGlyphs]
	// Advance value to be used for each glyph in dynamic glyph text.
	std::vector<SI16> FontAdvanceTable;
	// If FontFlagsHasLayout, RECT[NumGlyphs]
	// Not used in Flash Player through version 7 (always set to 0 to save space).
	std::vector<Rect> FontBoundsTable;
	// If FontFlagsHasLayout, KERNINGRECORD [KerningCount]
	// Not used in Flash Player through version 7 (omit with KerningCount of 0).
	std::vector<KerningRecord*> FontKerningTable;
public:
	TagDefineFont3();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFont4
	: public DefineTag
{
public:
	TagDefineFont4();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontInfo
	: public DefineTag
{
public:
	TagDefineFontInfo();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontInfo2
	: public DefineTag
{
public:
	TagDefineFontInfo2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontAlignZones
	: public SWFTag
{
public:
	SWFTag* fontCharacter;
	ByteArray bytes;
public:
	TagDefineFontAlignZones();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontName
	: public SWFTag
{
public:
	SWFTag* fontCharacter;
	std::string fontName;
	std::string fontCopyright;
public:
	TagDefineFontName();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineText
	: public DefineTag
{
public:
	// Bounds of the text.
	Rect TextBounds;
	// MATRIX
	// Transformation matrix for the text.
	Matrix TextMatrix;
	// UI8
	// Bits in each glyph index.
	UI8 GlyphBits;
	// UI8
	// Bits in each advance value.
	UI8 AdvanceBits;
	// TEXTRECORD[zero or more]  Text records.
	std::vector<TextRecord*> TextRecords;
	// UI8  Must be 0.
	UI8	EndOfRecordsFlag;
public:
	TagDefineText();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineText2
	: public DefineTag
{
public:
	TagDefineText2();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineEditText
	: public DefineTag
{
public:
	// Rectangle that completely encloses the text field.
	Rect bounds;
	// UB[1]
	// 0 = text field has no default text. 1 = text field initially displays the string specified by InitialText.
	UB HasText;
	// UB[1]
	// 0 = text will not wrap and will scroll sideways. 1 = text will wrap automatically when the end of line is reached.
	UB WordWrap;
	// UB[1]
	// 0 = text field is one line only. 1 = text field is multi-line and scrollable.
	UB Multiline;
	// UB[1]
	// 0 = characters are displayed as typed. 1 = all characters are displayed as an asterisk.
	UB Password;
	// UB[1]
	// 0 = text editing is enabled. 1 = text editing is disabled.
	UB ReadOnly;
	// UB[1]
	// 0 = use default color. 1 = use specified color (TextColor).
	UB HasTextColor;
	// UB[1]
	// 0 = length of text is unlimited. 1 = maximum length of string is specified by MaxLength.
	UB HasMaxLength;
	// UB[1]
	// 0 = use default font. 1 = use specified font (FontID) and height (FontHeight). (Can��t be true if HasFontClass is true).
	UB HasFont;
	// UB[1]
	// 0 = no fontClass, 1 = fontClass and Height specified for this text. (can't be true if HasFont is true). Supported in Flash Player 9.0.45.0 and later.
	UB HasFontClass;
	// UB[1]
	// 0 = fixed size. 1 = sizes to content (SWF 6 or later only).
	UB AutoSize;
	// UB[1]
	// Layout information provided.
	UB HasLayout;
	// UB[1]
	// Enables or disables interactive text selection.
	UB NoSelect;
	// UB[1]
	// Causes a border to be drawn around the text field.
	UB Border;
	// UB[1]
	// 0 = Authored as dynamic text; 1 = Authored as static text
	UB WasStatic;
	// UB[1]
	// 0 = plaintext content. 1 = HTML content (see following).
	UB HTML;
	// UB[1]
	// 0 = use device font. 1 = use glyph font.
	UB UseOutlines;
	// If HasFont
	// ID of font to use.
	UI16 FontId;
	DefineTag* font;
	// If HasFontClass, STRING 
	// Class name of font to be loaded from another SWF and used for this text.
	std::string FontClass;
	// If HasFont, UI16
	// Height of font in twips.
	UI16 FontHeight;
	// If HasTextColor, RGBA
	// Color of text.
	ColorRGBA TextColor;
	// If HasMaxLength, UI16
	// Text is restricted to this length.
	UI16 MaxLength;
	// If HasLayout, UI8
	// 0 = Left; 1 = Right; 2 = Center; 3 = Justify
	UI8 Align;
	// If HasLayout, UI16
	// Left margin in twips.
	UI16 LeftMargin;
	// If HasLayout, UI16
	// Right margin in twips.
	UI16 RightMargin;
	// If HasLayout, UI16
	// Indent in twips.
	UI16 Indent;
	// If HasLayout, SI16
	// Leading in twips (vertical distance between bottom of 
	// descender of one line and top of ascender of the next).
	SI16 Leading;
	// Name of the variable where the contents of the text field
	// are stored. May be qualified with dot syntax or slash syntax
	// for non-global variables.
	std::string VariableName;
	// If HasText STRING 
	// Text that is initially displayed.
	std::string InitialText;
public:
	TagDefineEditText();
	void visit(TagHandler* h) override;
};

class TagCSMTextSettings
	: public SWFTag
{
public:
	TagCSMTextSettings();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Fonts and Text

#pragma region The Display List
class TagShowFrame
	: public SWFTag
{
public:
	TagShowFrame();
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagPlaceObject
	: public SWFTag
{
public:
	UI16 Depth;
	Matrix matrix;
	CXFORM ColorTransform;
public:
	TagPlaceObject()
		: Depth(0)
	{

	}
	void visit(TagHandler* h) override;
};

class TagPlaceObject2
	: public TagPlaceObject
{
public:
	UI8 flags1;
	DefineTag* Character;
	UI16 Ratio;
	std::string Name;
	UI16 ClipDepth;
public:
	TagPlaceObject2()
		: flags1(0)
		, Character(nullptr)
		, Ratio(0)
		, ClipDepth(0)
	{

	}
	void visit(TagHandler* h) override;
public:
	inline bool hasClipAction()
	{
		return (flags1 & HAS_CLIP_ACTION) != 0;
	}

	inline bool hasClipDepth()
	{
		return (flags1 & HAS_CLIP_DEPTH) != 0;
	}

	inline void setClipDepth(UI16 depth)
	{
		this->ClipDepth = depth;
		flags1 |= HAS_CLIP_DEPTH;
	}

	inline bool hasMatrix()
	{
		return (flags1 & HAS_MATRIX) != 0;
	}

	inline void setMatrix(const Matrix& matrix)
	{
		this->matrix = matrix;
		flags1 |= HAS_MATRIX;
	}

	inline bool hasCxform()
	{
		return (flags1 & HAS_CXFORM) != 0;
	}

	inline bool hasName()
	{
		return (flags1 & HAS_NAME) != 0;
	}

	inline bool hasRatio()
	{
		return (flags1 & HAS_RATIO) != 0;
	}

	inline bool hasMove()
	{
		return (flags1 & HAS_MOVE) != 0;
	}

	inline bool hasCharID()
	{
		return (flags1 & HAS_CHARACTER) != 0;
	}

	inline void setCharacter(DefineTag* tag)
	{
		this->removeReference(this->Character);
		this->Character = tag;
		this->putReference(tag);
		if (tag == nullptr)
			flags1 &= ~HAS_CHARACTER;
		else
			flags1 |= HAS_CHARACTER;
	}
};

class TagPlaceObject3
	: public TagPlaceObject2
{
public:
	UI8 flags2;

	std::string ClassName;
	FilterList SurfaceFilterList;
	UI8 BlendMode;
	UI8 BitmapCache;
	UI8 Visible;
	ColorRGBA BackgroundColor;
public:
	TagPlaceObject3();
public:
	inline bool hasBlendMode()
	{
		return (flags2 & HAS_BLEND_MODE) != 0;
	}

	inline bool hasCacheAsBitmap()
	{
		return (flags2 & HAS_CACHE_AS_BITMAP) != 0;
	}

	inline bool hasImage()
	{
		return (flags2 & HAS_IMAGE) != 0;
	}

	inline bool hasClassName()
	{
		return (flags2 & HAS_CLASS_NAME) != 0;
	}

	inline bool hasFilterList()
	{
		return (flags2 & HAS_FILTER_LIST) != 0;
	}
public:
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

class TagRemoveObject2
	: public SWFTag
{
public:
	void visit(TagHandler* h) override;
};

class TagRemoveObject
	: public SWFTag
{
public:
	void visit(TagHandler* h) override;
};
#pragma endregion The Display List

#pragma region Images
class TagDefineBits
	: public DefineTag
{
public:
	ByteArray imageData;
};

class TagDefineBitsJPEG2
	: public TagDefineBits
{
public:
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};
class TagDefineBitsJPEG3
	: public TagDefineBitsJPEG2
{
public:
	ByteArray alphaData;
public:
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};
class TagDefineBitsLossless
	: public DefineTag
{
public:
	UI8 BitmapFormat;
	UI16 BitmapWidth;
	UI16 BitmapHeight;
	UI8 BitmapColorTableSize;
	ByteArray ZlibBitmapData;
public:
	TagDefineBitsLossless()
		: BitmapFormat(0)
		, BitmapWidth(0)
		, BitmapHeight(0)
	{

	}
	void visit(TagHandler* handler) override;
};
class TagDefineBitsLossless2
	: public TagDefineBitsLossless
{
public:
	void visit(TagHandler* handler) override;
};
#pragma endregion Images

#pragma region Meta data
class TagDefineBinaryData : public DefineTag
{
public:
	ByteArray binary;
public:
	void visit(TagHandler* h) override;
};
#pragma endregion Meta data

class TagUnknow : public SWFTag
{
public:
	TagUnknow(UI16 code);
	void visit(TagHandler* h) override;
	std::string getTagName() const override;
};
#endif // SWFTag_h__
