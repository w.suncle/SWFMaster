#include "TagDoABC.h"
#include "swf/SWFInputStream.h"
#include "swf/abc/ABCFile.h"
#include "../TagHandler.h"

void TagDoABC::setData( char* data, size_t len )
{
	SWFTag::setData(data, len);

	SWFInputStream stream(data, len);
	this->flags = stream.readUI32();
	this->name = stream.readString();

	this->_tagName = "DoABC(" + name + ")";
	this->abcFile = new ABCFile();
	this->abcFile->setData(data + stream.getPosition(), stream.getRemain());
	this->abcFile->analyze();
}

TagDoABC::TagDoABC()
	: abcFile(nullptr)
	, flags(0)
{

}

TagDoABC::~TagDoABC()
{
	if (abcFile != nullptr)
		delete abcFile;
}

void TagDoABC::visit( TagHandler* handler )
{
	handler->doABC(this);
}
