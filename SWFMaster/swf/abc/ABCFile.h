#ifndef ABCFile_h__
#define ABCFile_h__

#include "swf/SWFTypes.h"
#include <vector>

class ABCFile;
class CpoolInfo;
class SWFOutputStream;
class SWFInputStream;

#define NEED_ARGUMENTS 0x01 
#define NEED_ACTIVATION 0x02 
#define NEED_REST 0x04 
#define HAS_OPTIONAL 0x08 
#define SET_DXNS 0x40 
#define HAS_PARAM_NAMES 0x80 

#define ATTR_Final 0x1 
#define ATTR_Override 0x2 
#define ATTR_Metadata 0x4 

#define CONSTANT_ClassSealed 0x01 
#define CONSTANT_ClassFinal  0x02 
#define CONSTANT_ClassInterface  0x04 
#define CONSTANT_ClassProtectedNs 0x08 

enum class NamespaceKind
{
	Namespace = 0x08,
	PackageNamespace = 0x16,	//public
	PackageInternalNs = 0x17,	//internal
	ProtectedNamespace = 0x18,	//protected
	ExplicitNamespace = 0x19,
	StaticProtectedNs = 0x1A,	//static
	PrivateNs = 0x05			//private
};

enum class ConstantKind
{
	Int = 0x03,					//  integer
	UInt = 0x04,				//  uinteger
	Double = 0x06,				//  double
	Utf8 = 0x01,				//  string
	True = 0x0B,				//  -
	False = 0x0A,				//  -
	Null = 0x0C,				//  -
	Undefined = 0x00,			//  -
	Namespace = 0x08,			//  namespace
	PackageNamespace = 0x16,	//  namespace
	PackageInternalNs = 0x17,	//  Namespace
	ProtectedNamespace = 0x18,	//  Namespace
	ExplicitNamespace = 0x19,	//  Namespace
	StaticProtectedNs = 0x1A,	//  Namespace
	PrivateNs = 0x05,			//  namespace
};

enum class MultinameKind
{
	QName = 0x07,
	QNameA = 0x0D,
	RTQName = 0x0F,
	RTQNameA = 0x10,
	RTQNameL = 0x11,
	RTQNameLA = 0x12,
	Multiname = 0x09,
	MultinameA = 0x0E,
	MultinameL = 0x1B,
	MultinameLA = 0x1C,
	TypeName = 0x1D
};

enum class TraitKind
{
	Slot = 0,
	Method = 1,
	Getter = 2,
	Setter = 3,
	Class = 4,
	Function = 5,
	Const = 6
};

class CpoolItem
{
public:
	inline void setCpool(CpoolInfo* cpool) { _cpool = cpool; }
	inline CpoolInfo* getCpool() const { return _cpool; }
protected:
	CpoolInfo* _cpool;
};

class ABCFileItem
{
protected:
	ABCFile* _abcFile;
public:
	inline void setABCFile(ABCFile* abcFile) { _abcFile = abcFile; }
	inline ABCFile* getABCFile() const { return _abcFile; }
};

class NamespaceInfo
	: public CpoolItem
{
public:
	NamespaceKind kind;
	//string index
	UI30 name;
};

class NamespaceSetInfo
	: public CpoolItem
{
public:
	// ns is an integer that indexes into
	// the  namespace array of the constant pool.
	std::vector<UI30> ns;
};

class MultinameInfo
	: public CpoolItem
{
public:
	MultinameKind kind;
public:
	virtual std::string toString() const { return ""; };
	virtual void write(SWFOutputStream* stream) const = 0;
};

class QName
	: public MultinameInfo
{
public:
	// The  ns and  name fields are indexes into the  namespace and  string arrays of the  constant_pool entry,
	// respectively. A value of zero for the  ns field indicates the any (��*��) namespace, and a value of zero for the  name
	// field indicates the any (��*��) name.
	UI30 ns;
	// The  ns and  name fields are indexes into the  namespace and  string arrays of the  constant_pool entry,
	// respectively. A value of zero for the  ns field indicates the any (��*��) namespace, and a value of zero for the  name
	// field indicates the any (��*��) name.
	UI30 name;
public:
	void write(SWFOutputStream* stream) const override;
	std::string toString() const override;
};

class RTQName
	: public MultinameInfo
{
public:
	// The single field,  name , is an index into the  string array of the constant pool. A value of zero indicates the any
	// (��*��) name.
	UI30 name;
public:
	void write(SWFOutputStream* stream) const override;
};

class RTQNameL
	: public MultinameInfo
{
public:
	void write(SWFOutputStream* stream) const override;
};

class Multiname
	: public MultinameInfo
{
public:
	// The name  field is an index into the  string array
	// A value of zero for the  name field indicates the any (��*��) name.
	UI30 name;
	// The  ns_set field is an index into the  ns_set array. A
	// The value of  ns_set cannot be zero.
	UI30 nss;
public:
	void write(SWFOutputStream* stream) const override;
};

class MultinameL
	: public MultinameInfo
{
public:
	// The  ns_set field is an index into the  ns_set array of the constant pool. 
	// The value of  ns_set cannot be zero.
	UI30 nss;
public:
	void write(SWFOutputStream* stream) const override;
};

class TypeName
	: public MultinameInfo
{
public:
	UI30 name;
	UI30 count;
	std::vector<UI30> typeVec;
public:
	void write(SWFOutputStream* stream) const override;
};

class CpoolInfo
	: public ABCFileItem
{
public:
	std::vector<SI32> intVec;
	std::vector<UI32> uintVec;
	std::vector<DOUBLE> doubleVec;
	std::vector<std::string> stringVec;
	std::vector<NamespaceInfo*> namespaceInfoVec;
	std::vector<NamespaceSetInfo*> namespaceSetInfoVec;
	std::vector<MultinameInfo*> multinameInfoVec;
public:
	CpoolInfo();
	~CpoolInfo();
	void read(SWFInputStream* stream);
	void write(SWFOutputStream* stream);
	SI32 getInt(UI32 index) const;
	UI32 getUint(UI32 index) const;
	DOUBLE getDouble(UI32 index) const;
	std::string getString(UI32 index) const;
	NamespaceInfo* getNamespaceInfo(UI32 index) const;
	NamespaceSetInfo* getNamespaceSetInfo(UI32 index) const;
	MultinameInfo* getMultinameInfo(UI32 index) const;
private:
	void readIntegerVec(SWFInputStream* stream);
	void writeIntegerVec(SWFOutputStream* stream);

	void readUintVec(SWFInputStream* stream);
	void writeUintVec(SWFOutputStream* stream);
	
	void readDoubleVec(SWFInputStream* stream);
	void writeDoubleVec(SWFOutputStream* stream);
	
	void readStringVec(SWFInputStream* stream);
	void writeStringVec(SWFOutputStream* stream);
	
	void readNamespaceInfoVec(SWFInputStream* stream);
	void writeNamespaceInfoVec(SWFOutputStream* stream);
	
	void readNamespaceSetInfoVec(SWFInputStream* stream);
	void writeNamespaceSetInfoVec(SWFOutputStream* stream);
	
	void readMultinameInfoVec(SWFInputStream* stream);
	void writeMultinameInfoVec(SWFOutputStream* stream);
};

class MethodParam
{
public:
	// The type field is an index into the  multiname array of the constant pool; the name at that entry
	// provides the name of the return type of this method. A zero value denotes the any (��*��) type.
	UI30 type;
	// The name  field is an index into the  string array, and the  ns_set field is an index into the  ns_set array. A
	// value of zero for the  name field indicates the any (��*��) name. The value of  ns_set cannot be zero.
	UI30 name;
	bool hasDefaultValue;
	bool hasName;
	// Each optional value consists of a  kind field that denotes the type of value represented, and a  val field that is an
	// index into one of the array entries of the constant pool. The correct array is selected based on the kind.
	UI30 defaultVal;
	ConstantKind defaultKind;
public:
	MethodParam();
};

class MethodInfo
	: public ABCFileItem
{
public:
	// The  return_type field is an index into the  multiname array of the constant pool; the name at that entry
	// provides the name of the return type of this method. A zero value denotes the any (��*��) type.
	UI30 returnType;
	// The  name field is an index into the  string array of the constant pool; the string at that entry provides the
	// name of this method. If the index is zero, this method has no name.
	UI30 name;
	UI8 flags;
	// params
	std::vector<MethodParam*> paramVec;
public:
	void read(SWFInputStream* stream);
	void write(SWFOutputStream* stream);
	inline bool hasOptional() const { return (flags & HAS_OPTIONAL) == HAS_OPTIONAL; }
	inline bool hasParamNames() const { return (flags & HAS_PARAM_NAMES) == HAS_PARAM_NAMES; }
	std::string toString() const;
public:
	~MethodInfo();
};

class MetadataItem
{
public:
	UI30 key;
	UI30 value;
};

class MetadataInfo
{
public:
	UI30 name;
	std::vector<MetadataItem*> itemVec;
public:
	MetadataInfo();
	~MetadataInfo();
};

class TraitInfo
{
public:
	// The  name field is an index into the  multiname array of the constant pool; it provides a name for the trait.
	// The value can not be zero, and the  multiname entry specified must be a QName.
	UI30 name;
	TraitKind kind;
	UI8 attr;
	std::vector<UI30> metadataVec;
public:
	inline bool hasMetadata() const { return (attr & ATTR_Metadata) == ATTR_Metadata; }
public:
	static void readTraitInfoVec(std::vector<TraitInfo*>& traitInfoVec, SWFInputStream* stream);
};

class TraitSlot
	: public TraitInfo
{
public:
	UI30 slotId;
	UI30 typeName;
	UI30 vindex;
	UI30 vkind;
public:
	TraitSlot() : slotId(0), typeName(0), vindex(0), vkind(0) {}
};

class TraitClass
	: public TraitSlot
{
public:
	UI30 slotId;
	UI30 classIndex;
};

class TraitFunction
	: public TraitSlot
{
public:
	UI30 slotId;
	// The  function field is an index that points into the  method array of the  abcFile entry.
	UI30 functionIndex;
};

class TraitMethod
	: public TraitSlot
{
public:
	UI30 dispId;
	// The  method field is an index that points into the  method array of the  abcFile entry.
	UI30 methodIndex;
};

class InstanceInfo
	: public ABCFileItem
{
public:
	UI30 name;
	UI30 superName;
	UI8 flags;
	UI30 protectedNs;
	UI30 intrf_count;
	UI30 iinit;
	std::vector<UI30> interfaceVec;
	std::vector<TraitInfo*> traitInfoVec;
public:
	inline bool isInterface() const { return (flags & CONSTANT_ClassInterface) == CONSTANT_ClassInterface; }
	inline bool isFinal() const { return (flags & CONSTANT_ClassFinal) == CONSTANT_ClassFinal; }
};

class ClassInfo
	: public ABCFileItem
{
public:
	UI30 cinit;
	std::vector<TraitInfo*> traitInfoVec;
};

class ScriptInfo
	: public ABCFileItem
{
public:
	UI30 init;
	std::vector<TraitInfo*> traitInfoVec;
};

class MethodBodyInfo
	: public ABCFileItem
{
public:
	UI30 method;
	UI30 maxStack;
	UI30 localCount;
	UI30 initScopeDepth;
	UI30 maxScopeDepth;
	UI30 codeLength;
	char* code;

	std::vector<TraitInfo*> traitInfoVec;
public:
	MethodBodyInfo();
	~MethodBodyInfo();
};

class ABCFile
{
public:
	UI16 minorVersion;
	UI16 majorVersion;
	CpoolInfo* poolInfo;
	std::vector<MethodInfo*> methodInfoVec;
	std::vector<MetadataInfo*> metadataInfoVec;
	std::vector<InstanceInfo*> instanceInfoVec;
	std::vector<ClassInfo*> classInfoVec;
	std::vector<ScriptInfo*> scriptInfoVec;
	std::vector<MethodBodyInfo*> methodBodyInfoVec;
private:
	SWFInputStream* _stream;
	bool _isAnalyzed;
public:
	ABCFile();
	~ABCFile();

	void setData(char* data, size_t len);
	void write(SWFOutputStream* stream);
	void analyze();
	MethodBodyInfo* getMethodBodyInfo(int methodInfoIndex);
private:
	void readInstanceInfoVec(SWFInputStream* stream, UI30 class_count);
	void readClassInfoVec(SWFInputStream* stream, UI30 class_count);
	void readScriptInfoVec(SWFInputStream* stream);
	void readMethodBodyInfoVec(SWFInputStream* stream);
};
#endif // ABCFile_h__
