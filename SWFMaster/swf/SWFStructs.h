#ifndef SWFStruct_h__
#define SWFStruct_h__

#include "SWFTypes.h"
#include "ByteArray.h"
#include <string>
#include <vector>
#include <assert.h>

class DefineTag;
class SWFEncodeContext;
class SWFInputStream;
class SWFOutputStream;
class ShapeWithStyle;

class Matrix
{
public:
	UB HasScale;//UB 1
	UB NScaleBits;// If HasScale = 1, UB[5] Bits in each scale value field
	FB ScaleX;// If HasScale = 1, FB[NScaleBits]  x scale value
	FB ScaleY;// If HasScale = 1, FB[NScaleBits]  y scale value
	UB HasRotate;// UB[1] Has rotate and skew values if equal to 1
	UB NRotateBits;// If HasRotate = 1, UB[5] Bits in each rotate value field
	FB RotateSkew0;// If HasRotate = 1, FB[NRotateBits]  First rotate and skew value
	FB RotateSkew1;// If HasRotate = 1, FB[NRotateBits]  Second rotate and skew value
	UB NTranslateBits;// UB[5] Bits in each translate value field
	SB TranslateX;// SB[NTranslateBits] x translate value in twips
	SB TranslateY;//  SB[NTranslateBits] y translate value in twips
public:
	Matrix()
		: HasScale(0)
		, NScaleBits(0)
		, ScaleX(0)
		, ScaleY(0)
		, HasRotate(0)
		, NRotateBits(0)
		, RotateSkew0(0)
		, RotateSkew1(0)
		, NTranslateBits(0)
		, TranslateX(0)
		, TranslateY(0)
	{

	}
};


class CXFORM
{
public:
	UB HasAddTerms;//  UB[1]  Has color addition values if equal to 1
	UB HasMultTerms;// UB[1]  Has color multiply values if equal to 1
	UB Nbits;//  UB[4]  Bits in each value field
	SB RedMultTerm;//  If HasMultTerms = 1, SB[Nbits]  Red multiply value
	SB GreenMultTerm;//  If HasMultTerms = 1, SB[Nbits]  Green multiply value
	SB BlueMultTerm;//  If HasMultTerms = 1, SB[Nbits]  Blue multiply value
	SB RedAddTerm;//  If HasAddTerms = 1, SB[Nbits] Red addition value
	SB GreenAddTerm;//  If HasAddTerms = 1, SB[Nbits] Green addition value
	SB BlueAddTerm;// If HasAddTerms = 1, SB[Nbits]  Blue addition value
public:
	CXFORM()
		: HasAddTerms(0)
		, HasMultTerms(0)
		, Nbits(0)
		, RedMultTerm(0)
		, GreenMultTerm(0)
		, BlueMultTerm(0)
		, RedAddTerm(0)
		, GreenAddTerm(0)
		, BlueAddTerm(0)
	{

	}
};

class CXFORMWITHALPHA : public CXFORM
{
public:
	// If HasMultTerms = 1
	// SB[Nbits]  Alpha multiply value
	SB AlphaMultTerm;
	// If HasAddTerms = 1
	// SB[Nbits]  Transparency addition value
	SB AlphaAddTerm;
public:
	CXFORMWITHALPHA()
		: AlphaMultTerm(0)
		, AlphaAddTerm(0)
	{

	}
};

/* Type for Rectangle record. */
class Rect
{
public:
	UI32 NBits;
	SB Xmin;
	SB Xmax;
	SB Ymin;
	SB Ymax;
public:
	Rect()
		: NBits(0)
		, Xmin(0)
		, Xmax(0)
		, Ymin(0)
		, Ymax(0)
	{

	}
};

/* File structure */
class SWFHeader
{
public:
	UI8 Signature[3];
	UI8 Version;
	UI32 FileLength;
	Rect FrameSize;
	UI16 FrameRate;
	UI16 FrameCount;
public:
	SWFHeader()
		: Version(10)
	{
		FrameSize.Xmax = 400 * 20;
		FrameSize.Ymax = 300 * 20;
		FrameSize.NBits = 15;
	}
};

#pragma region filter
enum class FilterType
{
	DROPSHADOWFILTER = 0,
	BLURFILTER = 1,
	GLOWFILTER = 2,
	BEVELFILTER = 3,
	GRADIENTGLOWFILTER = 4,
	CONVOLUTIONFILTER = 5,
	COLORMATRIXFILTER = 6,
	GRADIENTBEVELFILTER = 7
};

class Filter
{
public:
	// 0 = Has DropShadowFilter
	// 1 = Has BlurFilter
	// 2 = Has GlowFilter
	// 3 = Has BevelFilter
	// 4 = Has GradientGlowFilter
	// 5 = Has ConvolutionFilter
	// 6 = Has ColorMatrixFilter
	// 7 = Has GradientBevelFilter
	UI8 FilterID;
public:
	virtual void decode(SWFInputStream* stream) = 0;
	virtual void encdoe(SWFOutputStream* stream) = 0;
};

class ColorMatrixFilter
	: public Filter
{
public:
	FLOAT matrix[20];
	ByteArray matrixData;
public:
	void decode(SWFInputStream* stream) override;
	void encdoe(SWFOutputStream* stream) override;
};

class ConvolutionFilter
	: public Filter
{
public:
	// Horizontal matrix size
	UI8 matrixX;
	// Vertical matrix size
	UI8 matrixY;
	// Divisor applied to the matrix values
	FLOAT divior;
	// Bias applied to the matrix values
	FLOAT bias;
	// FLOAT[MatrixX * MatrixY] 
	// Matrix values
	std::vector<FLOAT> matrix;
	// Default color for pixels outside the image
	ColorRGBA defaultColor;
	// Must be 0
	UB reserved;
	// Clamp mode
	UB clamp;
	// Preserve the alpha
	UB preserveAlpha;
public:
	void decode(SWFInputStream* stream) override;
	void encdoe(SWFOutputStream* stream) override;
};

class BlurFilter
	: public Filter
{
public:
	// Horizontal blur amount
	Fixed blurX;
	// Vertical blur amount
	Fixed blurY;
	// UB[5]
	// Number of blur passes
	UB passes;
	// UB[3]
	// Muest be 0
	UB reserved;
public:
	void decode(SWFInputStream* stream) override;
	void encdoe(SWFOutputStream* stream) override;
};

class FilterList
{
public:
	UI8 NumberOfFilters;
	std::vector<Filter*> list;
public:
	void decode(SWFInputStream* stream);
	void encode(SWFOutputStream* stream);
};

class GlowFilter
	: public Filter
{
public:
	// Color of the shadow
	ColorRGBA GolwColor;
	// Horizontal blur amount
	Fixed BlurX;
	// Vertical blur amount
	Fixed BlurY;
	// Strength of the glow
	Fixed8 Strength;
	// UB[1]
	// Inner glow mode
	UB InnerGlow;
	// UB[1]
	// Knockout mode
	UB Knockout;
	// UB[1]
	// Composite source. Always 1
	UB CompositeSource;
	// UB[5]
	// Number of blur passes
	UB Passes;
public:
	void decode(SWFInputStream* stream) override;
	void encdoe(SWFOutputStream* stream) override;
};
#pragma endregion filter

class GradRecord
{
public:
	UI8 Ratio;
	// RGB (Shape1 or Shape2)
	// RGBA (Shape3)
	ColorRGB Color;
public:
	GradRecord()
		: Color(0)
	{

	}
};

class Gradient
{
public:
	//UB[2]
	UB SpreadMode;
	//UB[2]
	UB InterpolationMode;
	//UB[4]
	UB NumGradients;

	std::vector<GradRecord*> GradientRecords;
public:
	virtual void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const;
};

class FocalGradient
	: public Gradient
{
public:
	Fixed8 FocalPoint;
public:
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const override;
};

class FillStyle
{
public:
	// Type of fill style:
	// 0x00 = solid fill
	// 0x10 = linear gradient fill
	// 0x12 = radial gradient fill
	// 0x13 = focal radial gradient fill (SWF8 file format and later only)
	// 0x40 = repeating bitmap fill
	// 0x41 = clipped bitmap fill
	// 0x42 = non-smoothed repeating bitmap
	// 0x43 = non-smoothed clipped bitmap
	UI8 FillStyleType;
	// If type = 0x00
	// RGBA (if Shape3);
	// RGB (if Shape1 or Shape2)
	ColorRGB Color;
	// If type = 0x10, 0x12, or 0x13
	Matrix GradientMatrix;
	// If type = 0x10 or 0x12, GRADIENT 
	// If type = 0x13, FOCALGRADIENT (SWF
	// 8 and later only)
	Gradient* Gradient;
	// If type = 0x40, 0x41, 0x42 or 0x43
	UI16 BitmapId;
	DefineTag* Bitmap;
	// If type = 0x40, 0x41, 0x42 or 0x43
	Matrix BitmapMatrix;
public:
	FillStyle()
		: Bitmap(nullptr)
		, Gradient(nullptr)
		, Color(0)
		, FillStyleType(0)
		, BitmapId(0xffff)
	{

	}
public:
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const;
};

class FillStyleArray
{
public:
	// If FillStyleCount = 0xFF
	UI16 FillStyleCountExtended;
	std::vector<FillStyle*> FillStyles;
public:
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const;
public:
	FillStyleArray()
		: FillStyleCountExtended(0)
	{

	}
};

class LineStyle
{
public:
	UI16 Width;
	// RGB (Shape1 or Shape2)
	// RGBA (Shape3)
	ColorRGB Color;
public:
	virtual void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const;
	LineStyle()
		: Width(0)
		, Color(0)
	{

	}
};

class LineStyle2
	: public LineStyle
{
public:
	// UB[2]
	UB StartCapStyle;
	// UB[2]
	UB JoinStyle;
	// UB[1]
	UB HasFillFlag;
	// UB[1]
	UB NoHScaleFlag;
	// UB[1]
	UB NoVScaleFlag;
	// UB[1]
	UB PixelHintingFlag;
	// UB[5]
	UB Reserved;
	// UB[1]
	UB NoClose;
	// UB[2]
	UB EndCapStyle;
	//If JoinStyle = 2
	UI16 MiterLimitFactor;
	// If HasFillFlag = 1
	FillStyle* FillType;
public:
	LineStyle2()
		: FillType(nullptr)
		, StartCapStyle(0)
		, JoinStyle(0)
		, HasFillFlag(0)
		, NoHScaleFlag(0)
		, NoVScaleFlag(0)
		, PixelHintingFlag(0)
		, Reserved(0)
		, NoClose(0)
		, EndCapStyle(0)
		, MiterLimitFactor(0)
	{

	}
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const override;
};

class LineStyleArray
{
public:
	// If LineStyleCount = 0xFF, UI16 
	UI16 LineStyleCountExtended;
	std::vector<LineStyle*> LineStyles;
public:
	LineStyleArray()
		: LineStyleCountExtended(0)
	{

	}
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt) const;
};

#pragma region Shape Records
class ShapeRecord
{
public:
	virtual void encode(SWFOutputStream* stream, SWFEncodeContext* cxt, ShapeWithStyle* style) const = 0;
};

class EndShapeRecord
	: public ShapeRecord
{
public:
	// UB[1]
	UB TypeFlag;
	// UB[5]
	UB EndOfShape;
public:
	EndShapeRecord()
		: TypeFlag(0)
		, EndOfShape(0)
	{

	}
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt, ShapeWithStyle* style) const override;
};

class StyleChangeRecord
	: public ShapeRecord
{
public:
	// UB[1]  Non-edge record flag. Always 0.
	UB TypeFlag;
	// UB[1]  New styles flag. Used by
	// DefineShape2 and DefineShape3
	// only.
	UB StateNewStyles;
	// UB[1]  Line style change flag.
	UB StateLineStyle;
	// UB[1]  Fill style 1 change flag.
	UB StateFillStyle1;
	// UB[1]  Fill style 0 change flag.
	UB StateFillStyle0;
	// UB[1]  Move to flag.
	UB StateMoveTo;
	// If StateMoveTo, UB[5] Move bit count.
	UB MoveBits;
	// If StateMoveTo, SB[MoveBits] Delta X value.
	UB MoveDeltaX;
	// If StateMoveTo, SB[MoveBits] Delta Y value.
	UB MoveDeltaY;
	// If StateFillStyle0, UB[FillBits] Fill 0 Style.
	UB FillStyle0;
	// If StateFillStyle1, UB[FillBits] Fill 1 Style.
	UB FillStyle1;
	// If StateLineStyle, UB[LineBits] Line Style.
	UB LineStyle;
	// If StateNewStyles, FILLSTYLEARRAY  Array of new fill styles.
	FillStyleArray* FillStyles;
	// If StateNewStyles, LINESTYLEARRAY  Array of new line styles.
	LineStyleArray* LineStyles;
	// If StateNewStyles, UB[4] Number of fill index bits for new
	// styles.
	UB NumFillBits;
	// If StateNewStyles, UB[4] Number of line index bits for new
	// styles.
	UB NumLineBits;
public:
	StyleChangeRecord()
		: FillStyles(nullptr)
		, LineStyles(nullptr)
		, TypeFlag(0)
		, StateNewStyles(0)
		, StateLineStyle(0)
		, StateFillStyle1(0)
		, StateFillStyle0(0)
		, StateMoveTo(0)
		, MoveBits(0)
		, MoveDeltaX(0)
		, MoveDeltaY(0)
		, FillStyle0(0)
		, FillStyle1(0)
		, LineStyle(0)
		, NumFillBits(0)
		, NumLineBits(0)
	{

	}
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt, ShapeWithStyle* style) const override;
};

class StraightEdgeRecord
	: public ShapeRecord
{
public:
	// UB[1] This is an edge record. Always 1.
	UB TypeFlag;
	// UB[1] Straight edge. Always 1.
	UB StraightFlag;
	// UB[4] Number of bits per value (2 less than the actual number).
	UB NumBits;
	// UB[1] General Line equals 1. Vert/Horz Line equals 0.
	UB GeneralLineFlag;
	// If GeneralLineFlag = 0, SB[1]  Vertical Line equals 1. Horizontal Line equals 0.
	UB VertLineFlag;
	// If GeneralLineFlag = 1 or if VertLineFlag = 0, SB[NumBits+2]
	// X delta
	UB DeltaX;
	// If GeneralLineFlag = 1 or if VertLineFlag = 1, SB[NumBits+2]
	// Y delta.
	UB DeltaY;
public:
	StraightEdgeRecord()
		: TypeFlag(1)
		, StraightFlag(1)
		, NumBits(0)
		, GeneralLineFlag(0)
		, VertLineFlag(0)
		, DeltaX(0)
		, DeltaY(0)
	{

	}
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt, ShapeWithStyle* style) const override;
};

class CurvedEdgeRecord
	: public ShapeRecord
{
public:
	// UB[1]  This is an edge record. Always 1.
	UB TypeFlag;
	// UB[1]  Curved edge. Always 0.
	UB StraightFlag;
	// UB[4]  Number of bits per value (2 less than the actual number).
	UB NumBits;
	// SB[NumBits+2]  X control point change.
	SB ControlDeltaX;
	// SB[NumBits+2]  Y control point change.
	SB ControlDeltaY;
	// SB[NumBits+2]  X anchor point change.
	SB AnchorDeltaX;
	// SB[NumBits+2]  Y anchor point change.
	SB AnchorDeltaY;
public:
	CurvedEdgeRecord()
		: TypeFlag(1)
		, StraightFlag(0)
		, NumBits(0)
		, ControlDeltaX(0)
		, ControlDeltaY(0)
		, AnchorDeltaX(0)
		, AnchorDeltaY(0)
	{

	}
	void encode(SWFOutputStream* stream, SWFEncodeContext* cxt, ShapeWithStyle* style) const override;
};
#pragma endregion Shape Records
// SHAPE is used by the DefineFont tag, to define character glyphs.
class Shape
{
public:
	// UB[4]
	// Number of fill index bits.
	UB NumFillBits;
	// UB[4]
	// Number of line index bits.
	UB NumLineBits;
	// SHAPERECORD[one or more]
	std::vector<ShapeRecord*> ShapeRecords;
};

class ShapeWithStyle
{
public:
	FillStyleArray* FillStyles;
	LineStyleArray* LineStyles;
	// UB[4]
	UB NumFillBits;
	// UB[4]
	UB NumLineBits;
	// ShapeRecords
	std::vector<ShapeRecord*> ShapeRecords;
public:
	ShapeWithStyle()
		: FillStyles(nullptr)
		, LineStyles(nullptr)
		, NumFillBits(0)
		, NumLineBits(0)
	{

	}
};

// A Kerning Record defines the distance between two glyphs in EM square coordinates. Certain pairs of glyphs
// appear more aesthetically pleasing if they are moved closer together, or farther apart. The FontKerningCode1
// and FontKerningCode2 fields are the character codes for the left and right characters. The
// FontKerningAdjustment field is a signed integer that defines a value to be added to the advance value of the left
// character.
class KerningRecord
{
public:
	// If FontFlagsWideCodes, UI16 Otherwise UI8
	// Character code of the left character.
	UI16 FontKerningCode1;
	// If FontFlagsWideCodes, UI16 Otherwise UI8
	// Character code of the right character.
	UI16 FontKerningCode2;
	//  SI16 Adjustment relative to left character��s advance value.
	SI16 FontKerningAdjustment;
public:
	KerningRecord()
		: FontKerningCode1(0)
		, FontKerningCode2(0)
		, FontKerningAdjustment(0)
	{

	}
};

class ActionRecord
{
public:

};

class ButtonRecord
{
public:
	// UB[2]
	// Reserved bits; always 0
	UB ButtonReserved;
	// UB[1]
	// 0 = No blend mode; 1 = Has blend mode (SWF 8 and later only)
	UB ButtonHasBlendMode;
	// UB[1]
	// 0 = No filter list; 1 = Has filter list (SWF 8 and later only)
	UB ButtonHasFilterList;
	// UB[1]
	// Present in hit test state 
	UB ButtonStateHitTest;
	// UB[1]
	// Present in down state 
	UB ButtonStateDown;
	// UB[1]
	// Present in over state 
	UB ButtonStateOver;
	// UB[1]
	// Present in up state 
	UB ButtonStateUp;
	// UI16
	// Depth at which to place character
	UI16 PlaceDepth;
	// MATRIX
	// Transformation matrix for character placement
	Matrix PlaceMatrix;
	// If within DefineButton2
	// Character color transform
	CXFORMWITHALPHA ColorTransform;
	// If within DefineButton2 and ButtonHasFilterList = 1, FILTERLIST
	// List of filters on this button
	FilterList FilterList;
	// If within DefineButton2 and ButtonHasBlendMode = 1, UI8
	// 0 or 1 = normal; 2 = layer; 3 = multiply; 4 = 
	// screen; 5 = lighten; 6 = darken; 7 = difference; 
	// 8 = add; 9 = subtract; 10 = invert; 11 = alpha; 
	// 12 = erase; 13 = overlay; 14 = hardlight; 
	// Values 15 to 255 are reserved.
	UI8 BlendMode;

	DefineTag* character;
public:
	ButtonRecord()
		: character(nullptr)
		, ButtonReserved(0)
		, ButtonHasBlendMode(0)
		, ButtonHasFilterList(0)
		, ButtonStateHitTest(0)
		, ButtonStateDown(0)
		, ButtonStateOver(0)
		, ButtonStateUp(0)
		, PlaceDepth(0)

	{

	}
};

class GlyphEntry
{
public:
	// Glyph index into current font.
	UB GlyphIndex;
	// x advance value for glyph.
	SB GlyphAdvance;
};

class TextRecord
{
public:
	UI8 flags;
	// UB[1]
	// Always 1.
	UB TextRecordType;
	// UB[3]
	// Always 0.
	UB StyleFlagsReserved;
	// UB[1]
	// 1 if text font specified.
	UB StyleFlagsHasFont;
	// UB[1] 1
	// if text color specified.
	UB StyleFlagsHasColor;
	// UB[1] 1
	// if y offset specified.
	UB StyleFlagsHasYOffset;
	// UB[1] 1
	// if x offset specified.
	UB StyleFlagsHasXOffset;
	// If StyleFlagsHasFont, UI16 Font ID for following text.
	UI16 FontID;
	DefineTag* fontCharacter;
	// If StyleFlagsHasColor, RGB If this record is part of a DefineText2 tag, RGBA
	// Font color for following text.
	ColorRGB TextColor;
	// If StyleFlagsHasXOffset, SI16 x offset for following text.
	SI16 XOffset;
	// If StyleFlagsHasYOffset, SI16 y offset for following text.
	SI16 YOffset;
	// If hasFont, UI16 Font height for following text.
	UI16 TextHeight;
	// UI8
	// Number of glyphs in record.
	UI8 GlyphCount;
	// GLYPHENTRY[GlyphCount] Glyph entry (see following).
	std::vector<GlyphEntry*> GlyphEntries;
};
#endif // SWFTag_h__
