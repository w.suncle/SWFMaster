#ifndef ImageHelper_h__
#define ImageHelper_h__

#include <QImage>

class TagDefineBits;
class TagDefineBitsJPEG3;

class ImageHelper
{
public:
	static TagDefineBits* createDefineBits(QImage* image);
	static TagDefineBitsJPEG3* createDefineBitsJPEG3(QImage* image);
};
#endif // ImageHelper_h__
