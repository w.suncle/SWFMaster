#-------------------------------------------------
#
# Project created by QtCreator 2014-04-04T00:59:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SWFMaster
TEMPLATE = app

SOURCES += main.cpp\
        SWFMasterWindow.cpp \
    lzma/Alloc.c \
    lzma/Bcj2.c \
    lzma/Bra.c \
    lzma/Bra86.c \
    lzma/BraIA64.c \
    lzma/CpuArch.c \
    lzma/Delta.c \
    lzma/LzFind.c \
    lzma/LzFindMt.c \
    lzma/Lzma2Dec.c \
    lzma/Lzma2Enc.c \
    lzma/Lzma86Dec.c \
    lzma/Lzma86Enc.c \
    lzma/LzmaDec.c \
    lzma/LzmaEnc.c \
    lzma/LzmaLib.c \
    lzma/MtCoder.c \
    lzma/Ppmd7.c \
    lzma/Ppmd7Dec.c \
    lzma/Ppmd7Enc.c \
    lzma/Sha256.c \
    lzma/Threads.c \
    swf/SWF.cpp \
    swf/SWFInputStream.cpp \
    swf/SWFReader.cpp \
    swf/SWFTag.cpp \
    swf/SWFTagFactory.cpp \
    swf/tags/TagDoABC.cpp \
    swf/tags/TagMetadata.cpp \
    swf/tags/TagUnknow.cpp \
    swf/abc/ABCFile.cpp \
    swf/abc/Opcode.cpp \
    editor/model/DoABCTreeItem.cpp \
    editor/MetaDataEditor.cpp \
    editor/DoABCEditor.cpp \
    editor/Editor.cpp \
    editor/CpoolInfoEditor.cpp \
    editor/OpcodeEditor.cpp \
    editor/tags/DefineBitsJPEG3Editor.cpp

HEADERS  += SWFMasterWindow.h \
    lzma/Alloc.h \
    lzma/Bcj2.h \
    lzma/Bra.h \
    lzma/CpuArch.h \
    lzma/Delta.h \
    lzma/LzFind.h \
    lzma/LzFindMt.h \
    lzma/LzHash.h \
    lzma/Lzma2Dec.h \
    lzma/Lzma2Enc.h \
    lzma/Lzma86.h \
    lzma/LzmaDec.h \
    lzma/LzmaEnc.h \
    lzma/LzmaLib.h \
    lzma/MtCoder.h \
    lzma/Ppmd.h \
    lzma/Ppmd7.h \
    lzma/RotateDefs.h \
    lzma/Sha256.h \
    lzma/Threads.h \
    lzma/Types.h \
    swf/SWF.h \
    swf/SWFInputStream.h \
    swf/SWFReader.h \
    swf/SWFTag.h \
    swf/SWFTagFactory.h \
    swf/SWFTypes.h \
    swf/tags/TagDoABC.h \
    swf/tags/TagMetadata.h \
    swf/tags/TagUnknow.h \
    swf/abc/ABCFile.h \
    swf/abc/Opcode.h \
    editor/model/DoABCTreeItem.h \
    editor/MetaDataEditor.h \
    editor/DoABCEditor.h \
    editor/Editor.h \
    editor/CpoolInfoEditor.h \
    editor/OpcodeEditor.h

FORMS    += SWFMasterWindow.ui \
    editor/MetaDataEditor.ui \
    editor/CpoolInfoEditor.ui \
    editor/OpcodeEditor.ui \
    editor/tags/DefineBitsJPEG3Editor.ui

RESOURCES += \
    icons.qrc

OTHER_FILES += \
    resources/style.css \
    resources/darkOrange.css
