#ifndef Editor_h__
#define Editor_h__

#include <QWidget>
#include "qhexedit.h"

class Editor
{
public:
	virtual const QString getEditorName() const = 0;
	virtual QWidget* asWidget() const = 0;
};

class DefaultHexEditor
	: public QHexEdit
	, public Editor
{
public:
	DefaultHexEditor(QWidget* parent);
	virtual const QString getEditorName() const override;
	virtual QWidget* asWidget() const override;
};
#endif // Editor_h__
