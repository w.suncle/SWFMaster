#include "Icon.h"


QIcon* Icon::getIcon( Kind kind, int ns /*= IconTrait::kPublic*/ )
{
	QIcon* icon = nullptr;
	int key = 0;
	switch (kind)
	{
	case Icon::kInterface:
		key = kind * 1000;
		break;
	case Icon::kClass:
	case Icon::kMethod:
	case Icon::kField:
	case Icon::kGetter:
	case Icon::kSetter:
		key = kind * 1000 + ns;
		break;
	default:
		assert(false);
		break;
	}
	QString keyStr = QString("%1").arg(key);
	auto itr = _cachedIconMap.find(keyStr);
	if (itr == _cachedIconMap.end())
	{
		QString baseIconFile;
		switch (kind)
		{
		case Icon::kClass:
			icon = new QIcon(":/resources/icon/as3/class_obj.png");
			break;
		case Icon::kInterface:
			icon = new QIcon(":/resources/icon/as3/int_obj.png");
			break;
		case Icon::kMethod:
			{
				if (isTrait(ns, IconTrait::kPublic))
					baseIconFile = ":/resources/icon/as3/methpub_obj.png";
				else if (isTrait(ns, IconTrait::kPrivate))
					baseIconFile = ":/resources/icon/as3/methpri_obj.png";
				else if (isTrait(ns, IconTrait::kProtected))
					baseIconFile = ":/resources/icon/as3/methpro_obj.png";
			}
			break;
		case Icon::kField:
			{
				if (isTrait(ns, IconTrait::kPublic))
					baseIconFile = ":/resources/icon/as3/field_public_obj.png";
				else if (isTrait(ns, IconTrait::kPrivate))
					baseIconFile = ":/resources/icon/as3/field_private_obj.png";
				else if (isTrait(ns, IconTrait::kProtected))
					baseIconFile = ":/resources/icon/as3/field_protected_obj.png";
			}
			break;
		case Icon::kGetter:
		case Icon::kSetter:
			{
				if (isTrait(ns, IconTrait::kPublic))
					baseIconFile = ":/resources/icon/as3/methgettersetter_public_obj.png";
				else if (isTrait(ns, IconTrait::kPrivate))
					baseIconFile = ":/resources/icon/as3/methgettersetter_private_obj.png";
				else if (isTrait(ns, IconTrait::kProtected))
					baseIconFile = ":/resources/icon/as3/methgettersetter_protected_obj.png";
			}
			break;
		default:
			break;
		}

		if (icon == nullptr)
		{
			QPixmap backgroud(baseIconFile);
			QPainter painter(&backgroud);
			//left top
			if (isTrait(ns, IconTrait::kFinal))
			{
				QPixmap bFinal(":/resources/icon/as3/final_co.png");
				painter.drawPixmap(QRect(0, 0, bFinal.width(), bFinal.height()), bFinal);
			}
			//right top
			if (isTrait(ns, IconTrait::kStatic))
			{
				QPixmap bStatic(":/resources/icon/as3/static_co.png");
				painter.drawPixmap(QRect(backgroud.width() - bStatic.width(), 0, bStatic.width(), bStatic.height()), bStatic);
			}
			//right bottom
			if (isTrait(ns, IconTrait::kOverride))
			{
				QPixmap bOverride(":/resources/icon/as3/over_co.png");
				painter.drawPixmap(QRect(backgroud.width() - bOverride.width(), backgroud.height() - bOverride.height(), bOverride.width(), bOverride.height()), bOverride);
			}
			if (isTrait(ns, IconTrait::kConst))
			{
				QPixmap bConst(":/resources/icon/as3/const_co.png");
				painter.drawPixmap(QRect(backgroud.width() - bConst.width(), backgroud.height() - bConst.height(), bConst.width(), bConst.height()), bConst);
			}
			icon = new QIcon(backgroud);
		}
		_cachedIconMap[keyStr] = icon;
	}
	else
	{
		icon = itr->second;
	}

	return icon;
}

bool Icon::isTrait( int n, IconTrait trait )
{
	return (n & trait) == trait;
}