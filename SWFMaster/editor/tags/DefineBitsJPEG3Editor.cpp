#include "DefineBitsJPEG3Editor.h"
#include "ui_DefineBitsJPEG3Editor.h"
#include <QPainter>
#include <QGraphicsPixmapItem>
#include <QFileDialog>

DefineBitsJPEG3Editor::DefineBitsJPEG3Editor(TagDefineBitsJPEG3* tag, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DefineBitsJPEG3Editor),
	_tag(tag)
{
    ui->setupUi(this);

	/*
	QImage* image = new QImage();
	bool suc = image->loadFromData((uchar*)tag->data+6, tag->length-6);
	
	QGraphicsScene *scene = new QGraphicsScene;
	QGraphicsPixmapItem *item = new QGraphicsPixmapItem;
	item->setPixmap(QPixmap::fromImage(*image));
	scene->addItem(item);
	item->setPos(0,0);
	ui->graphicsView->setScene(scene);
	*/
}

DefineBitsJPEG3Editor::~DefineBitsJPEG3Editor()
{
    delete ui;
}

void DefineBitsJPEG3Editor::onReplace()
{
	QString file = QFileDialog::getOpenFileName(this, "Select Picture", QString());
	QFile f(file);
	if (f.exists())
	{
		f.open(QIODevice::OpenModeFlag::ReadOnly);
		QByteArray ba = f.readAll();
		_tag->imageData.copy(ba.data(), ba.length());
		
		QImage* image = new QImage(file);
		
		QGraphicsScene *scene = new QGraphicsScene;
		QGraphicsPixmapItem *item = new QGraphicsPixmapItem;
		item->setPixmap(QPixmap::fromImage(*image));
		scene->addItem(item);
		item->setPos(0,0);
		ui->graphicsView->setScene(scene);
	}
}

const QString DefineBitsJPEG3Editor::getEditorName() const 
{
	return "img";
}

QWidget* DefineBitsJPEG3Editor::asWidget() const 
{
	return (QWidget*) this;
}
