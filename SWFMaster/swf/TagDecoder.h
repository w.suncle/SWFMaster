#ifndef TagDecoder_h__
#define TagDecoder_h__

#include "TagHandler.h"
#include "SWFStructs.h"
#include <map>

class SWF;
class SWFFrame;

class TagDecoder
	: TagHandler
{
private:
	SWFInputStream* _tagStream;
	SWFInputStream* _inputStream;
	TagHandler* _tagHandler;
	TagHandler* _defineTagManager;
private:
	Shape*			decodeShape(UB tagCode, SWFTag* tag);
	ShapeWithStyle*	decodeShapeWithStyle(UB tagCode, SWFTag* tag);
	FillStyleArray*	decodeFillStyleArray(UB tagCode, SWFTag* tag);
	LineStyleArray*	decodeLineStyleArray(UB tagCode, SWFTag* tag);
	FillStyle*		decodeFillStyle(UB tagCode, SWFTag* tag);
	Gradient*		decodeGradient(UB tagCode, SWFTag* tag);
	LineStyle*		decodeLineStyle(UB tagCode, SWFTag* tag);
public:
	TagDecoder(SWFInputStream* stream, TagHandler* tagHandler, TagHandler* defineTagManager = nullptr);
	~TagDecoder();

	void decode();
private:
	DefineTag* getDefineTag(UI16 charId) const override;

	virtual void unknow( TagUnknow* tag );

	virtual void doABC( TagDoABC* tag );

	virtual void metadata( TagMetadata* tag );

	virtual void frameLabel( TagFrameLabel* tag );

	virtual void setBackgroundColor( TagSetBackgroundColor* tag );

	virtual void fileAttributes( TagFileAttributes* tag );

	virtual void end( TagEnd* tag );

	virtual void scriptLimits( TagScriptLimits* tag );

	virtual void symbolClass( TagSymbolClass* tag );

	virtual void importAssets2( TagImportAssets2* tag );

	virtual void protect( TagProtect* tag );

	virtual void exportAssets( TagExportAssets* tag );

	virtual void enableDebugger( TagEnableDebugger* tag );

	virtual void enableDebugger2( TagEnableDebugger2* tag );

	virtual void setTabIndex( TagSetTabIndex* tag );

	virtual void defineScalingGrid( TagDefineScalingGrid* tag );

	virtual void defineSceneAndFrameLabelData( TagDefineSceneAndFrameLabelData* tag );

	virtual void defineShape( TagDefineShape* tag );

	virtual void defineShape2( TagDefineShape2* tag );

	virtual void defineShape3( TagDefineShape3* tag );

	virtual void defineShape4( TagDefineShape4* tag );

	virtual void defineButton( TagDefineButton* tag );

	virtual void defineButton2( TagDefineButton2* tag );

	virtual void defineButtonCxform( TagDefineButtonCxform* tag );

	virtual void defineButtonSound( TagDefineButtonSound* tag );

	virtual void defineSprite( TagDefineSprite* tag );

	virtual void defineMorphShape( TagDefineMorphShape* tag );

	virtual void defineMorphShape2( TagDefineMorphShape2* tag );

	virtual void defineFont( TagDefineFont* tag );

	virtual void defineFont2( TagDefineFont2* tag );

	virtual void defineFont3( TagDefineFont3* tag );

	virtual void defineFont4( TagDefineFont4* tag );

	virtual void defineFontInfo( TagDefineFontInfo* tag );

	virtual void defineFontInfo2( TagDefineFontInfo2* tag );

	virtual void defineFontAlignZones( TagDefineFontAlignZones* tag );

	virtual void defineFontName( TagDefineFontName* tag );

	virtual void defineText( TagDefineText* tag );

	virtual void defineText2( TagDefineText2* tag );

	virtual void defineEditText( TagDefineEditText* tag );

	virtual void cSMTextSettings( TagCSMTextSettings* tag );

	virtual void showFrame( TagShowFrame* tag );

	virtual void placeObject( TagPlaceObject* tag );

	virtual void placeObject2( TagPlaceObject2* tag );

	virtual void defineBitsJPEG2( TagDefineBitsJPEG2* tag );

	virtual void defineBitsJPEG3( TagDefineBitsJPEG3* tag );

	virtual void defineBinaryData( TagDefineBinaryData* tag );

	virtual void productInfo( TagProductInfo* tag );

	virtual void placeObject3( TagPlaceObject3* tag );

	virtual void removeObject(TagRemoveObject* tag);

	virtual void removeObject2(TagRemoveObject2* tag);

	virtual void defineBitsLossless( TagDefineBitsLossless* tag );

	virtual void defineBitsLossless2( TagDefineBitsLossless2* tag );

	virtual void defineSound( TagDefineSound* tag );

};

class TagDefineSprite;

class DefineSpriteDecoder
	: public TagHandler
{
private:
	TagDefineSprite* _tag;
public:
	DefineSpriteDecoder(TagDefineSprite* tag)
		: _tag(tag)
	{

	}
private:
	virtual void any(SWFTag* tag) override;

	virtual void unknow(TagUnknow* tag) override {};

	virtual void doABC(TagDoABC* tag) override {};

	virtual void metadata(TagMetadata* tag) override {};

	virtual void frameLabel(TagFrameLabel* tag) override {};

	virtual void setBackgroundColor(TagSetBackgroundColor* tag) override {};

	virtual void fileAttributes(TagFileAttributes* tag) override {};

	virtual void end(TagEnd* tag) override {};

	virtual void scriptLimits(TagScriptLimits* tag) override {};

	virtual void symbolClass(TagSymbolClass* tag) override {};

	virtual void importAssets2(TagImportAssets2* tag) override {};

	virtual void protect(TagProtect* tag) override {};

	virtual void exportAssets(TagExportAssets* tag) override {};

	virtual void enableDebugger(TagEnableDebugger* tag) override {};

	virtual void enableDebugger2(TagEnableDebugger2* tag) override {};

	virtual void setTabIndex(TagSetTabIndex* tag) override {};

	virtual void defineScalingGrid(TagDefineScalingGrid* tag) override {};

	virtual void defineSceneAndFrameLabelData(TagDefineSceneAndFrameLabelData* tag) override {};

	virtual void defineShape(TagDefineShape* tag) override {};

	virtual void defineShape2(TagDefineShape2* tag) override {};

	virtual void defineShape3(TagDefineShape3* tag) override {};

	virtual void defineShape4(TagDefineShape4* tag) override {};

	virtual void defineButton(TagDefineButton* tag) override {};

	virtual void defineButton2(TagDefineButton2* tag) override {};

	virtual void defineButtonCxform(TagDefineButtonCxform* tag) override {};

	virtual void defineButtonSound(TagDefineButtonSound* tag) override {};

	virtual void defineSprite(TagDefineSprite* tag) override {};

	virtual void defineMorphShape(TagDefineMorphShape* tag) override {};

	virtual void defineMorphShape2(TagDefineMorphShape2* tag) override {};

	virtual void defineFont(TagDefineFont* tag) override {};

	virtual void defineFont2(TagDefineFont2* tag) override {};

	virtual void defineFont3(TagDefineFont3* tag) override {};

	virtual void defineFont4(TagDefineFont4* tag) override {};

	virtual void defineFontInfo(TagDefineFontInfo* tag) override {};

	virtual void defineFontInfo2(TagDefineFontInfo2* tag) override {};

	virtual void defineFontAlignZones(TagDefineFontAlignZones* tag) override {};

	virtual void defineFontName(TagDefineFontName* tag) override {};

	virtual void defineText(TagDefineText* tag) override {};

	virtual void defineText2(TagDefineText2* tag) override {};

	virtual void defineEditText(TagDefineEditText* tag) override {};

	virtual void cSMTextSettings(TagCSMTextSettings* tag) override {};

	virtual void showFrame(TagShowFrame* tag) override {};

	virtual void placeObject(TagPlaceObject* tag) override {};

	virtual void placeObject2(TagPlaceObject2* tag) override {};

	virtual void placeObject3(TagPlaceObject3* tag) override {};

	virtual void defineBitsJPEG2(TagDefineBitsJPEG2* tag) override {};

	virtual void defineBitsJPEG3(TagDefineBitsJPEG3* tag) override {};

	virtual void defineBitsLossless(TagDefineBitsLossless* tag) override {};

	virtual void defineBitsLossless2(TagDefineBitsLossless2* tag) override {};

	virtual void defineBinaryData(TagDefineBinaryData* tag) override {};

	virtual void productInfo(TagProductInfo* tag) override {};

	virtual void removeObject(TagRemoveObject* tag) override {};

	virtual void removeObject2(TagRemoveObject2* tag) override {};

	virtual void defineSound( TagDefineSound* tag ) override {};

};
#endif // TagDecoder_h__
