#ifndef TagHandler_h__
#define TagHandler_h__

#include "swf/tags/TagDoABC.h"

class TagHandler
{
public:
	virtual DefineTag* getDefineTag(UI16 charId) const { return nullptr; };
public:
	virtual void any(SWFTag* tag) {};
	virtual void unknow(TagUnknow* tag) = 0;
	virtual void doABC(TagDoABC* tag) = 0;
	virtual void metadata(TagMetadata* tag) = 0;
	virtual void frameLabel(TagFrameLabel* tag) = 0;
	virtual void setBackgroundColor(TagSetBackgroundColor* tag) = 0;
	virtual void fileAttributes(TagFileAttributes* tag) = 0;
	virtual void end(TagEnd* tag) = 0;
	virtual void scriptLimits(TagScriptLimits* tag) = 0;
	virtual void symbolClass(TagSymbolClass* tag) = 0;
	virtual void importAssets2(TagImportAssets2* tag) = 0;
	virtual void protect(TagProtect* tag) = 0;
	virtual void exportAssets(TagExportAssets* tag) = 0;
	virtual void enableDebugger(TagEnableDebugger* tag) = 0;
	virtual void enableDebugger2(TagEnableDebugger2* tag) = 0;
	virtual void setTabIndex(TagSetTabIndex* tag) = 0;
	virtual void defineScalingGrid(TagDefineScalingGrid* tag) = 0;
	virtual void defineSceneAndFrameLabelData(TagDefineSceneAndFrameLabelData* tag) = 0;
	virtual void defineShape(TagDefineShape* tag) = 0;
	virtual void defineShape2(TagDefineShape2* tag) = 0;
	virtual void defineShape3(TagDefineShape3* tag) = 0;
	virtual void defineShape4(TagDefineShape4* tag) = 0;
	virtual void defineButton(TagDefineButton* tag) = 0;
	virtual void defineButton2(TagDefineButton2* tag) = 0;
	virtual void defineButtonCxform(TagDefineButtonCxform* tag) = 0;
	virtual void defineButtonSound(TagDefineButtonSound* tag) = 0;
	virtual void defineSprite(TagDefineSprite* tag) = 0;
	virtual void defineMorphShape(TagDefineMorphShape* tag) = 0;
	virtual void defineMorphShape2(TagDefineMorphShape2* tag) = 0;
	virtual void defineFont(TagDefineFont* tag) = 0;
	virtual void defineFont2(TagDefineFont2* tag) = 0;
	virtual void defineFont3(TagDefineFont3* tag) = 0;
	virtual void defineFont4(TagDefineFont4* tag) = 0;
	virtual void defineFontInfo(TagDefineFontInfo* tag) = 0;
	virtual void defineFontInfo2(TagDefineFontInfo2* tag) = 0;
	virtual void defineFontAlignZones(TagDefineFontAlignZones* tag) = 0;
	virtual void defineFontName(TagDefineFontName* tag) = 0;
	virtual void defineText(TagDefineText* tag) = 0;
	virtual void defineText2(TagDefineText2* tag) = 0;
	virtual void defineEditText(TagDefineEditText* tag) = 0;
	virtual void cSMTextSettings(TagCSMTextSettings* tag) = 0;
	virtual void showFrame(TagShowFrame* tag) = 0;
	virtual void placeObject(TagPlaceObject* tag) = 0;
	virtual void placeObject2(TagPlaceObject2* tag) = 0;
	virtual void placeObject3(TagPlaceObject3* tag) = 0;
	virtual void defineBitsJPEG2(TagDefineBitsJPEG2* tag) = 0;
	virtual void defineBitsJPEG3(TagDefineBitsJPEG3* tag) = 0;
	virtual void defineBitsLossless(TagDefineBitsLossless* tag) = 0;
	virtual void defineBitsLossless2(TagDefineBitsLossless2* tag) = 0;
	virtual void defineBinaryData(TagDefineBinaryData* tag) = 0;
	virtual void productInfo(TagProductInfo* tag) = 0;
	virtual void removeObject(TagRemoveObject* tag) = 0;
	virtual void removeObject2(TagRemoveObject2* tag) = 0;
	virtual void defineSound(TagDefineSound* tag) = 0;
};
#endif // TagHandler_h__
