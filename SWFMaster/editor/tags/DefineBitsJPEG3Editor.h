#ifndef DEFINEBITSJPEG3EDITOR_H
#define DEFINEBITSJPEG3EDITOR_H

#include <QWidget>
#include "swf/SWFTag.h"
#include "../Editor.h"

namespace Ui {
class DefineBitsJPEG3Editor;
}

class DefineBitsJPEG3Editor
	: public QWidget
	, public Editor
{
    Q_OBJECT

public:
    explicit DefineBitsJPEG3Editor(TagDefineBitsJPEG3* tag, QWidget *parent = 0);
	~DefineBitsJPEG3Editor();
	virtual const QString getEditorName() const override;
	virtual QWidget* asWidget() const override;
private slots:
	void onReplace();
private:
    Ui::DefineBitsJPEG3Editor *ui;
	TagDefineBitsJPEG3* _tag;
};

#endif // DEFINEBITSJPEG3EDITOR_H
