#ifndef TagBuilder_H__
#define TagBuilder_H__

class SWFTag;
class DefineTag;

class TagBuilder
{
public:
	virtual SWFTag* build() = 0;
};

class PlaceObjectBuilder
	: public TagBuilder
{
private:
	DefineTag* _tag;
public:
	PlaceObjectBuilder(DefineTag* tag)
		: _tag(tag)
	{

	}
	SWFTag* build();
};

#endif