#include "SWF.h"
#include "SWFTag.h"

SWF::~SWF()
{
	for (SWFTag* tag : tags)
	{
		delete tag;
	}
	for (auto frame : frames)
	{
		delete frame;
	}
}

SWF::SWF()
	: fileAttributes(nullptr)
	, enableDebugger(nullptr)
	, enableDebugger2(nullptr)
	, metadata(nullptr)
	, scriptLimits(nullptr)
	, setBackgroundColor(nullptr)
	, defineSceneAndFrameLabelData(nullptr)
	, productInfo(nullptr)
	, protect(nullptr)
	, version(13)
	, frameRate(30)
	, width(400)
	, height(300)
{

}

void SWF::getImageTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineBits:
		case DefineBitsJPEG2:
		case DefineBitsJPEG3:
		case DefineBitsJPEG4:
		case DefineBitsLossless:
		case DefineBitsLossless2:
			output.push_back(tag);
			break;
		}
	}
}

void SWF::getShapeTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineShape:
		case DefineShape2:
		case DefineShape3:
		case DefineShape4:
			output.push_back(tag);
			break;
		}
	}
}

void SWF::getSpriteTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineSprite:
			output.push_back(tag);
			break;
		}
	}
}

void SWF::getFontTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineFont:
		case DefineFont2:
		case DefineFont3:
		case DefineFont4:
			output.push_back(tag);
			break;
		}
	}
}

void SWF::getTextTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineText:
		case DefineText2:
			output.push_back(tag);
			break;
		}
	}
}

void SWF::getButtonTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineButton:
		case DefineButton2:
			output.push_back(tag);
			break;
		}
	}
}

void SWF::getOtherTags( std::vector<SWFTag*>& output ) const
{
	for (SWFTag* tag : tags)
	{
		switch (tag->code)
		{
		case DefineBits:
		case DefineBitsJPEG2:
		case DefineBitsJPEG3:
		case DefineBitsJPEG4:
		case DefineBitsLossless:
		case DefineBitsLossless2:

		case DefineShape:
		case DefineShape2:
		case DefineShape3:
		case DefineShape4:

		case DefineButton:
		case DefineButton2:

		case DefineSprite:

		case DefineFont:
		case DefineFont2:
		case DefineFont3:
		case DefineFont4:

		case DefineText:
		case DefineText2:
			break;
		default:
			output.push_back(tag);
			break;
		}
	}
}

SWFFrame::SWFFrame()
	: frameLabel(nullptr)
	, symbolClass(nullptr)
{

}

void SWFFrame::getAllDefineTags(std::vector<DefineTag*>& tags)
{
	tags.insert(tags.end(), exportDefs.begin(), exportDefs.end());
}
