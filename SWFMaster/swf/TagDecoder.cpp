#include "lzma/LzmaLib.h"
#include "TagDecoder.h"
#include "SWF.h"
#include <assert.h>
#include "QtZlib/zlib.h"
#include "SWFTagFactory.h"

TagDecoder::TagDecoder(SWFInputStream* stream, TagHandler* handler, TagHandler* defineTagManager)
	: _inputStream(stream)
	, _tagHandler(handler)
	, _defineTagManager(defineTagManager)
{
	if (_defineTagManager == nullptr)
		_defineTagManager = handler;

	_tagStream = new SWFInputStream();
}

TagDecoder::~TagDecoder()
{
	if(_tagStream)
		delete _tagStream;
}

void TagDecoder::decode()
{
	//READ TAGS
	SWFTagFactory tagFactory;
	int tagIdx = 0;
	do 
	{
		UI16 tagCodeAndLength = _inputStream->readUI16();
		UI16 tagCode = (tagCodeAndLength >> 6) & 0x3FF;
		UI32 tagLen = tagCodeAndLength & 0x3F;
		if (tagLen >= 63)
		{
			tagLen = _inputStream->readUI32();
		}
		char* tagData = (char*) malloc(tagLen);
		_inputStream->readBytes(tagData, tagLen);
		_tagStream->setData(tagData, tagLen);

		SWFTag* tag = tagFactory.create(tagCode);
		tag->code = tagCode;
		tag->length = tagLen;
		tag->setData(tagData, tagLen);
		tag->visit(this);
		_tagHandler->any(tag);
		tag->visit(_tagHandler);
		
		tagIdx++;
	} while (_inputStream->getRemain() > 0);
}

Shape* TagDecoder::decodeShape(UB tagCode, SWFTag* tag)
{
	Shape* shape = new Shape();
	
	_tagStream->alignBits();
	shape->NumFillBits = _tagStream->readUB(4);
	shape->NumLineBits = _tagStream->readUB(4);

	bool endRecord = false;
	do 
	{
		UB type = _tagStream->readUB(1);
		if (type == 0)
		{
			UB StateNewStyles = _tagStream->readUB(1);
			UB StateLineStyle = _tagStream->readUB(1); 
			UB StateFillStyle1 = _tagStream->readUB(1);
			UB StateFillStyle0 = _tagStream->readUB(1);
			UB StateMoveTo = _tagStream->readUB(1);
			//StyleChangeRecord
			if (StateNewStyles == 0 &&
				StateLineStyle == 0 &&
				StateFillStyle0 == 0 &&
				StateFillStyle1 == 0 &&
				StateMoveTo == 0)
			{
				endRecord = true;
			}
			else
			{
				StyleChangeRecord* r = new StyleChangeRecord();
				r->TypeFlag = type;
				r->StateNewStyles = StateNewStyles;
				r->StateLineStyle = StateLineStyle;
				r->StateFillStyle0 = StateFillStyle0;
				r->StateFillStyle1 = StateFillStyle1;
				r->StateMoveTo = StateMoveTo;

				if (r->StateMoveTo)
				{
					r->MoveBits = _tagStream->readUB(5);
					r->MoveDeltaX = _tagStream->readSB(r->MoveBits);
					r->MoveDeltaY = _tagStream->readSB(r->MoveBits);
				}

				if (r->StateFillStyle0)
				{
					r->FillStyle0 = _tagStream->readUB(shape->NumFillBits);
				}

				if (r->StateFillStyle1)
				{
					r->FillStyle1 = _tagStream->readUB(shape->NumFillBits);
				}

				if (r->StateLineStyle)
				{
					r->LineStyle = _tagStream->readUB(shape->NumLineBits);
				}

				if (r->StateNewStyles)
				{
					r->FillStyles = this->decodeFillStyleArray(tagCode, tag);
					r->LineStyles = this->decodeLineStyleArray(tagCode, tag);
					_tagStream->alignBits();
					r->NumFillBits = _tagStream->readUB(4);
					r->NumLineBits = _tagStream->readUB(4);
					shape->NumFillBits = r->NumFillBits;
					shape->NumLineBits = r->NumLineBits;
				}
				shape->ShapeRecords.push_back(r);
			}
		}
		else
		{
			UB StraightFlag = _tagStream->readUB(1);
			if (StraightFlag == 1)
			{
				// line
				StraightEdgeRecord* r = new StraightEdgeRecord();
				r->TypeFlag = type;
				r->StraightFlag = StraightFlag;
				r->NumBits = _tagStream->readUB(4) + 2;
				r->GeneralLineFlag = _tagStream->readUB(1);
				if (r->GeneralLineFlag == 0)
					r->VertLineFlag = _tagStream->readUB(1);
				if (r->GeneralLineFlag == 1 || r->VertLineFlag == 0)
					r->DeltaX = _tagStream->readSB(r->NumBits);
				if (r->GeneralLineFlag == 1 || r->VertLineFlag == 1)
					r->DeltaY = _tagStream->readSB(r->NumBits);

				shape->ShapeRecords.push_back(r);
			}
			else
			{
				// curve
				CurvedEdgeRecord* r = new CurvedEdgeRecord();
				r->TypeFlag = type;
				r->StraightFlag = StraightFlag;
				r->NumBits = _tagStream->readUB(4) + 2;
				r->ControlDeltaX = _tagStream->readSB(r->NumBits);
				r->ControlDeltaY = _tagStream->readSB(r->NumBits);
				r->AnchorDeltaX = _tagStream->readSB(r->NumBits);
				r->AnchorDeltaY = _tagStream->readSB(r->NumBits);
				shape->ShapeRecords.push_back(r);
			}
		}
	} while (!endRecord);
	return shape;
}

ShapeWithStyle* TagDecoder::decodeShapeWithStyle(UB tagCode, SWFTag* tag)
{
	_tagStream->alignBits();

	ShapeWithStyle* shape = new ShapeWithStyle();
	shape->FillStyles = decodeFillStyleArray(tagCode, tag);
	shape->LineStyles = decodeLineStyleArray(tagCode, tag);

	_tagStream->alignBits();
	shape->NumFillBits = _tagStream->readUB(4);
	shape->NumLineBits = _tagStream->readUB(4);

	bool endRecord = false;
	do 
	{
		UB type = _tagStream->readUB(1);
		if (type == 0)
		{
			UB StateNewStyles = _tagStream->readUB(1);
			UB StateLineStyle = _tagStream->readUB(1); 
			UB StateFillStyle1 = _tagStream->readUB(1);
			UB StateFillStyle0 = _tagStream->readUB(1);
			UB StateMoveTo = _tagStream->readUB(1);
			//StyleChangeRecord
			if (StateNewStyles == 0 &&
				StateLineStyle == 0 &&
				StateFillStyle0 == 0 &&
				StateFillStyle1 == 0 &&
				StateMoveTo == 0)
			{
				endRecord = true;
			}
			else
			{
				StyleChangeRecord* r = new StyleChangeRecord();
				r->TypeFlag = type;
				r->StateNewStyles = StateNewStyles;
				r->StateLineStyle = StateLineStyle;
				r->StateFillStyle0 = StateFillStyle0;
				r->StateFillStyle1 = StateFillStyle1;
				r->StateMoveTo = StateMoveTo;

				if (r->StateMoveTo)
				{
					r->MoveBits = _tagStream->readUB(5);
					r->MoveDeltaX = _tagStream->readSB(r->MoveBits);
					r->MoveDeltaY = _tagStream->readSB(r->MoveBits);
				}

				if (r->StateFillStyle0)
				{
					r->FillStyle0 = _tagStream->readUB(shape->NumFillBits);
				}

				if (r->StateFillStyle1)
				{
					r->FillStyle1 = _tagStream->readUB(shape->NumFillBits);
				}

				if (r->StateLineStyle)
				{
					r->LineStyle = _tagStream->readUB(shape->NumLineBits);
				}

				if (r->StateNewStyles)
				{
					r->FillStyles = this->decodeFillStyleArray(tagCode, tag);
					r->LineStyles = this->decodeLineStyleArray(tagCode, tag);
					_tagStream->alignBits();
					r->NumFillBits = _tagStream->readUB(4);
					r->NumLineBits = _tagStream->readUB(4);
					shape->NumFillBits = r->NumFillBits;
					shape->NumLineBits = r->NumLineBits;
				}
				shape->ShapeRecords.push_back(r);
			}
		}
		else
		{
			UB StraightFlag = _tagStream->readUB(1);
			if (StraightFlag == 1)
			{
				// line
				StraightEdgeRecord* r = new StraightEdgeRecord();
				r->TypeFlag = type;
				r->StraightFlag = StraightFlag;
				r->NumBits = _tagStream->readUB(4) + 2;
				r->GeneralLineFlag = _tagStream->readUB(1);
				if (r->GeneralLineFlag == 0)
					r->VertLineFlag = _tagStream->readUB(1);
				if (r->GeneralLineFlag == 1 || r->VertLineFlag == 0)
					r->DeltaX = _tagStream->readSB(r->NumBits);
				if (r->GeneralLineFlag == 1 || r->VertLineFlag == 1)
					r->DeltaY = _tagStream->readSB(r->NumBits);

				shape->ShapeRecords.push_back(r);
			}
			else
			{
				// curve
				CurvedEdgeRecord* r = new CurvedEdgeRecord();
				r->TypeFlag = type;
				r->StraightFlag = StraightFlag;
				r->NumBits = _tagStream->readUB(4) + 2;
				r->ControlDeltaX = _tagStream->readSB(r->NumBits);
				r->ControlDeltaY = _tagStream->readSB(r->NumBits);
				r->AnchorDeltaX = _tagStream->readSB(r->NumBits);
				r->AnchorDeltaY = _tagStream->readSB(r->NumBits);
				shape->ShapeRecords.push_back(r);
			}
		}
	} while (!endRecord);
	return shape;
}

FillStyleArray* TagDecoder::decodeFillStyleArray(UB tagCode, SWFTag* tag)
{
	FillStyleArray* arr = new FillStyleArray();
	UI8 count = _tagStream->readUI8();
	if (count == 0xff)
		arr->FillStyleCountExtended = _tagStream->readUI16();
	for (UI8 i = 0; i < count; ++i)
	{
		arr->FillStyles.push_back(decodeFillStyle(tagCode, tag));
	}

	return arr;
}

FillStyle* TagDecoder::decodeFillStyle(UB tagCode, SWFTag* tag)
{
	FillStyle* style = new FillStyle();
	style->FillStyleType = _tagStream->readUI8();

	//Color
	if (style->FillStyleType == 0x00)
	{
		switch (tagCode)
		{
		case DefineShape:
		case DefineShape2:
			style->Color = _tagStream->readColorRGB();
			break;
		case DefineShape3:
		case DefineShape4:
			style->Color = _tagStream->readColorRGBA();
			break;
		}
	}

	//GradientMatrix
	if (style->FillStyleType == 0x10 ||
		style->FillStyleType == 0x12 ||
		style->FillStyleType == 0x13)
	{
		style->GradientMatrix = _tagStream->readMatrix();
	}

	//Gradient
	if (style->FillStyleType == 0x10 ||
		style->FillStyleType == 0x12)
	{
		_tagStream->alignBits();

		Gradient* g = new Gradient();
		g->SpreadMode = _tagStream->readUB(2);
		g->InterpolationMode = _tagStream->readUB(2);
		g->NumGradients = _tagStream->readUB(4);
		for (int i = 0; i < g->NumGradients; ++i)
		{
			GradRecord* gr = new GradRecord();
			gr->Ratio = _tagStream->readUI8();
			gr->Color = (tagCode == DefineShape3 || tagCode == DefineShape4) ? _tagStream->readColorRGBA() : _tagStream->readColorRGB();
			g->GradientRecords.push_back(gr);
		}
		style->Gradient = g;
	}
	else if (style->FillStyleType == 0x13)
	{
		_tagStream->alignBits();

		//FocalGradient;
		FocalGradient* g = new FocalGradient();
		g->SpreadMode = _tagStream->readUB(2);
		g->InterpolationMode = _tagStream->readUB(2);
		g->NumGradients = _tagStream->readUB(4);
		for (int i = 0; i < g->NumGradients; ++i)
		{
			GradRecord* gr = new GradRecord();
			gr->Ratio = _tagStream->readUI8();
			gr->Color = (tagCode == DefineShape3 || tagCode == DefineShape4) ? _tagStream->readColorRGBA() : _tagStream->readColorRGB();
			g->GradientRecords.push_back(gr);
		}
		g->FocalPoint = _tagStream->readFixed8();
		style->Gradient = g;
	}

	//Bitmap id
	if (style->FillStyleType == 0x40 ||
		style->FillStyleType == 0x41 ||
		style->FillStyleType == 0x42 ||
		style->FillStyleType == 0x43)
	{
		style->BitmapId = _tagStream->readUI16();
		style->Bitmap = this->getDefineTag(style->BitmapId);
		style->BitmapMatrix = _tagStream->readMatrix();
		tag->putReference(style->Bitmap);
	}

	return style;
}

LineStyleArray* TagDecoder::decodeLineStyleArray(UB tagCode, SWFTag* tag)
{
	LineStyleArray* arr = new LineStyleArray();
	UI8 LineStyleCount = _tagStream->readUI8();
	if (LineStyleCount == 0xff)
	{
		arr->LineStyleCountExtended = _tagStream->readUI16();
	}

	for (UI8 i = 0; i < LineStyleCount; ++i)
	{
		arr->LineStyles.push_back(decodeLineStyle(tagCode, tag));
	}

	return arr;
}

LineStyle* TagDecoder::decodeLineStyle(UB tagCode, SWFTag* tag)
{
	if (tagCode == DefineShape4)
	{
		LineStyle2* style = new LineStyle2();
		style->Width = _tagStream->readUI16();
		style->StartCapStyle = _tagStream->readUB(2);
		style->JoinStyle = _tagStream->readUB(2);
		style->HasFillFlag = _tagStream->readUB(1);
		style->NoHScaleFlag = _tagStream->readUB(1);
		style->NoVScaleFlag = _tagStream->readUB(1);
		style->PixelHintingFlag = _tagStream->readUB(1);
		UB Reserved = _tagStream->readUB(5);
		style->NoClose = _tagStream->readUB(1);
		style->EndCapStyle = _tagStream->readUB(2);
		if (style->JoinStyle == 2)
			style->MiterLimitFactor = _tagStream->readUI16();
		if (style->HasFillFlag == 0)
			style->Color = _tagStream->readColorRGBA();
		else
			style->FillType = decodeFillStyle(tagCode, tag);

		return style;
	}
	else
	{
		LineStyle* style = new LineStyle();
		style->Width = _tagStream->readUI16();
		if (tagCode == DefineShape3)
			style->Color = _tagStream->readColorRGBA();
		else
			style->Color = _tagStream->readColorRGB();
		return style;
	}
}

void TagDecoder::doABC( TagDoABC* tag )
{
}

DefineTag* TagDecoder::getDefineTag(UI16 charId) const
{
	return _defineTagManager->getDefineTag(charId);
}

void TagDecoder::unknow( TagUnknow* tag )
{
	
}

void TagDecoder::metadata( TagMetadata* tag )
{
	tag->text = _tagStream->readString();
}

void TagDecoder::frameLabel( TagFrameLabel* tag )
{
	tag->name = _tagStream->readString();
}

void TagDecoder::setBackgroundColor( TagSetBackgroundColor* tag )
{
	tag->color = _tagStream->readColorRGB();
}

void TagDecoder::fileAttributes( TagFileAttributes* tag )
{
	UI8 flags = _tagStream->readUI8();

	tag->UseDirectBlit = (flags & (1 << 1)) != 0;
	tag->UseGPU = (flags & (1 << 2)) != 0;
	tag->HasMetadata = (flags & (1 << 3)) != 0;
	tag->ActionScript3 = (flags & (1 << 4)) != 0;
	tag->UseNetwork = (flags & (1 << 7)) != 0;
}

void TagDecoder::end( TagEnd* tag )
{
	
}

void TagDecoder::scriptLimits( TagScriptLimits* tag )
{

}

void TagDecoder::symbolClass( TagSymbolClass* tag )
{
	UI16 num = _tagStream->readUI16();
	for (UI16 i = 0; i < num; ++i)
	{
		UI16 refid = _tagStream->readUI16();
		auto name = _tagStream->readString();
		if (refid == 0)
		{
			tag->topLevelClass = name;
		}
		else
		{
			SWFTag* ref = this->getDefineTag(refid);
			tag->tags[name] = ref;
		}
	}
}

void TagDecoder::importAssets2( TagImportAssets2* tag )
{
	
}

void TagDecoder::protect( TagProtect* tag )
{
}

void TagDecoder::exportAssets( TagExportAssets* tag )
{
	
}

void TagDecoder::enableDebugger( TagEnableDebugger* tag )
{
}

void TagDecoder::enableDebugger2( TagEnableDebugger2* tag )
{
	
}

void TagDecoder::setTabIndex( TagSetTabIndex* tag )
{

}

void TagDecoder::defineScalingGrid( TagDefineScalingGrid* tag )
{
	tag->characterID = _tagStream->readUI16();
	tag->Splitter = _tagStream->readRect();
}

void TagDecoder::defineSceneAndFrameLabelData( TagDefineSceneAndFrameLabelData* tag )
{
	//TODO: �浽Tag ��
	UI32 sceneCount = _tagStream->readU32();
	for (UI32 i = 0; i < sceneCount; ++i)
	{
		UI32 offset = _tagStream->readU32();
		std::string name = _tagStream->readString();
	}
	UI32 frameCount = _tagStream->readU32();
	for (UI32 i = 0; i < frameCount; ++i)
	{
		UI32 offset = _tagStream->readU32();
		std::string name = _tagStream->readString();
	}
}

void TagDecoder::defineShape( TagDefineShape* tag )
{
	tag->characterID = _tagStream->readUI16();
	tag->ShapeBounds = _tagStream->readRect();
	tag->Shapes = this->decodeShapeWithStyle(tag->code, tag);
}

void TagDecoder::defineShape2( TagDefineShape2* tag )
{
	this->defineShape(tag);
}

void TagDecoder::defineShape3( TagDefineShape3* tag )
{
	this->defineShape(tag);
}

void TagDecoder::defineShape4( TagDefineShape4* tag )
{
	tag->characterID = _tagStream->readUI16();
	tag->ShapeBounds = _tagStream->readRect();
	tag->EdgeBounds = _tagStream->readRect();
	_tagStream->alignBits();
	UB Reserved = _tagStream->readUB(5);
	tag->UsesFillWindingRule = _tagStream->readUB(1);
	tag->UsesNonScalingStrokes = _tagStream->readUB(1);
	tag->UsesScalingStrokes = _tagStream->readUB(1);
	tag->Shapes = this->decodeShapeWithStyle(tag->code, tag);
}

void TagDecoder::defineButton( TagDefineButton* tag )
{
	assert(false);
}

void TagDecoder::defineButton2( TagDefineButton2* tag )
{
	tag->characterID = _tagStream->readUI16();
	UB ReservedFlags = _tagStream->readUB(7);
	tag->TrackAsMenu = _tagStream->readUB(1);
	tag->ActionOffset = _tagStream->readUI16();

	//Buttonrecord
	do 
	{
		_tagStream->alignBits();

		ButtonRecord* record = new ButtonRecord();
		record->ButtonReserved = _tagStream->readUB(2);
		record->ButtonHasBlendMode = _tagStream->readUB(1);
		record->ButtonHasFilterList = _tagStream->readUB(1);
		record->ButtonStateHitTest = _tagStream->readUB(1);
		record->ButtonStateDown = _tagStream->readUB(1);
		record->ButtonStateOver = _tagStream->readUB(1);
		record->ButtonStateUp = _tagStream->readUB(1);
		if (record->ButtonHasBlendMode == 0 &&
			record->ButtonHasFilterList == 0 &&
			record->ButtonStateHitTest == 0 &&
			record->ButtonStateDown == 0 &&
			record->ButtonStateOver == 0 &&
			record->ButtonStateUp == 0)
		{
			delete record;
			break;
		}
		
		UI16 characterId = _tagStream->readUI16();
		record->character = this->getDefineTag(characterId);
		tag->putReference(record->character);

		record->PlaceDepth = _tagStream->readUI16();
		record->PlaceMatrix = _tagStream->readMatrix();
		record->ColorTransform = _tagStream->readColorTransformWithAlpha();
		if (record->ButtonHasFilterList)
		{
			record->FilterList.decode(_tagStream);
		}
		if (record->ButtonHasBlendMode)
		{
			record->BlendMode = _tagStream->readUI8();
		}
		tag->Characters.push_back(record);
	} while (true);

	_tagStream->readByteArray(&tag->characterData, _tagStream->getRemain());

	//TODO: BUTTONCONDACTION
	//while (_tagStream->getRemain())
	//{
	//	assert(false);
	//}
}

void TagDecoder::defineButtonCxform( TagDefineButtonCxform* tag )
{
	tag->ButtonId = _tagStream->readUI16();
	tag->putReference(this->getDefineTag(tag->ButtonId));
	tag->ButtonColorTransform = _tagStream->readColorTransform();
}

void TagDecoder::defineButtonSound( TagDefineButtonSound* tag )
{
	assert(false);
}

void TagDecoder::defineSprite( TagDefineSprite* tag )
{
	tag->characterID = _tagStream->readUI16();
	tag->frameCount = _tagStream->readUI16();

	DefineSpriteDecoder spriteDecoder(tag);
	TagDecoder tagDecoder(_tagStream, &spriteDecoder, _defineTagManager);
	tagDecoder.decode();
}

void TagDecoder::defineMorphShape( TagDefineMorphShape* tag )
{
	//assert(false);
}

void TagDecoder::defineMorphShape2( TagDefineMorphShape2* tag )
{
	assert(false);
}

void TagDecoder::defineFont( TagDefineFont* tag )
{
	assert(false);
}

void TagDecoder::defineFont2( TagDefineFont2* tag )
{
	assert(false);
}

void TagDecoder::defineFont3( TagDefineFont3* tag )
{
	tag->characterID = _tagStream->readUI16();
	_tagStream->readByteArray(&tag->characterData, _tagStream->getRemain());
	/*
	tag->FontFlagsHasLayout = _tagStream->readUB(1);
	tag->FontFlagsShiftJIS = _tagStream->readUB(1);
	tag->FontFlagsSmallText = _tagStream->readUB(1);
	tag->FontFlagsANSI = _tagStream->readUB(1);
	tag->FontFlagsWideOffsets = _tagStream->readUB(1);
	tag->FontFlagsWideCodes = _tagStream->readUB(1);
	tag->FontFlagsItalic = _tagStream->readUB(1);
	tag->FontFlagsBold = _tagStream->readUB(1);

	tag->LanguageCode = _tagStream->readUI8();
	tag->FontNameLen = _tagStream->readUI8();
	tag->FontName = _tagStream->readString(tag->FontNameLen);

	tag->NumGlyphs = _tagStream->readUI16();
	for (UI16 i = 0; i < tag->NumGlyphs; ++i)
	{
		if (tag->FontFlagsWideOffsets)
			tag->OffsetTable.push_back(_tagStream->readUI32());
		else
			tag->OffsetTable.push_back(_tagStream->readUI16());
	}

	if (tag->FontFlagsWideOffsets)
		tag->CodeTableOffset = _tagStream->readUI32();
	else
		tag->CodeTableOffset = _tagStream->readUI16();

	for (UI16 i = 0; i < tag->NumGlyphs; ++i)
	{
		Shape* shape = this->decodeShape(DefineShape3);
		tag->GlyphShapeTable.push_back(shape);
	}

	for (UI16 i = 0; i < tag->NumGlyphs; ++i)
	{
		tag->CodeTable.push_back(_tagStream->readUI16());
	}

	if (tag->FontFlagsHasLayout)
	{
		assert(false);
	}
	*/
}

void TagDecoder::defineFont4( TagDefineFont4* tag )
{
	assert(false);
}

void TagDecoder::defineFontInfo( TagDefineFontInfo* tag )
{
	assert(false);
}

void TagDecoder::defineFontInfo2( TagDefineFontInfo2* tag )
{
	assert(false);
}

void TagDecoder::defineFontAlignZones( TagDefineFontAlignZones* tag )
{
	UI16 id = _tagStream->readUI16();

	tag->fontCharacter = this->getDefineTag(id);
	tag->putReference(tag->fontCharacter);
	_tagStream->readByteArray(&tag->bytes, _tagStream->getRemain());
}

void TagDecoder::defineFontName( TagDefineFontName* tag )
{
	UI16 fontCharacterId = _tagStream->readUI16();
	tag->fontCharacter = this->getDefineTag(fontCharacterId);
	tag->fontName = _tagStream->readString();
	tag->fontCopyright = _tagStream->readString();

	tag->putReference(tag->fontCharacter);
}

void TagDecoder::defineText( TagDefineText* tag )
{
	tag->characterID = _tagStream->readUI16();

	tag->TextBounds = _tagStream->readRect();
	tag->TextMatrix = _tagStream->readMatrix();
	tag->GlyphBits = _tagStream->readUI8();
	tag->AdvanceBits = _tagStream->readUI8();
	
	do 
	{
		UI8 flags = _tagStream->readUI8();
		if (flags == 0)
			break;

		TextRecord* rec = new TextRecord();
		rec->flags = flags;
		rec->TextRecordType = 1;
		rec->StyleFlagsReserved = 0;
		rec->StyleFlagsHasFont = (flags >> 3) & 1;
		rec->StyleFlagsHasColor = (flags >> 2) & 1;
		rec->StyleFlagsHasYOffset = (flags >> 1) & 1;
		rec->StyleFlagsHasXOffset = (flags >> 0) & 1;
		
		if (rec->StyleFlagsHasFont)
		{
			UI16 charId = _tagStream->readUI16();
			rec->fontCharacter = this->getDefineTag(charId);
			tag->putReference(rec->fontCharacter);
		}
		if (rec->StyleFlagsHasColor)
		{
			rec->TextColor = _tagStream->readColorRGB();
		}
		if (rec->StyleFlagsHasXOffset)
		{
			rec->XOffset = _tagStream->readSI16();
		}
		if (rec->StyleFlagsHasYOffset)
		{
			rec->YOffset = _tagStream->readSI16();
		}
		if (rec->StyleFlagsHasFont)
		{
			rec->TextHeight = _tagStream->readUI16();
		}
		rec->GlyphCount = _tagStream->readUI8();
		_tagStream->alignBits();
		for (UI8 i = 0; i < rec->GlyphCount; ++i)
		{
			GlyphEntry* entry = new GlyphEntry();
			entry->GlyphIndex = _tagStream->readUB(tag->GlyphBits);
			entry->GlyphAdvance = _tagStream->readSB(tag->AdvanceBits);
			rec->GlyphEntries.push_back(entry);
		}

		tag->TextRecords.push_back(rec);

	} while (true);
}

void TagDecoder::defineText2( TagDefineText2* tag )
{
	assert(false);
	//TODO: tagRef
	//TODO: addDefineTag
}

void TagDecoder::defineEditText( TagDefineEditText* tag )
{
	tag->characterID = _tagStream->readUI16();
	
	tag->bounds = _tagStream->readRect();
	_tagStream->alignBits();

	tag->HasText = _tagStream->readUB(1);
	tag->WordWrap = _tagStream->readUB(1);
	tag->Multiline = _tagStream->readUB(1);
	tag->Password = _tagStream->readUB(1);
	tag->ReadOnly = _tagStream->readUB(1);
	tag->HasTextColor = _tagStream->readUB(1);
	tag->HasMaxLength = _tagStream->readUB(1);
	tag->HasFont = _tagStream->readUB(1);
	tag->HasFontClass = _tagStream->readUB(1);
	tag->AutoSize = _tagStream->readUB(1);
	tag->HasLayout = _tagStream->readUB(1);
	tag->NoSelect = _tagStream->readUB(1);
	tag->Border = _tagStream->readUB(1);
	tag->WasStatic = _tagStream->readUB(1);
	tag->HTML = _tagStream->readUB(1);
	tag->UseOutlines = _tagStream->readUB(1);

	if (tag->HasFont)
	{
		tag->FontId = _tagStream->readUI16();
		tag->font = this->getDefineTag(tag->FontId);
		tag->putReference(tag->font);
	}
	if (tag->HasFontClass)
	{
		tag->FontClass = _tagStream->readString();
	}
	if (tag->HasFont)
	{
		tag->FontHeight = _tagStream->readUI16();
	}
	if (tag->HasTextColor)
	{
		tag->TextColor = _tagStream->readColorRGBA();
	}
	if (tag->HasMaxLength)
	{
		tag->MaxLength = _tagStream->readUI16();
	}
	if (tag->HasLayout)
	{
		tag->Align = _tagStream->readUI8();
		tag->LeftMargin = _tagStream->readUI16();
		tag->RightMargin = _tagStream->readUI16();
		tag->Indent = _tagStream->readUI16();
		tag->Leading = _tagStream->readSI16();
	}
	tag->VariableName = _tagStream->readString();
	if (tag->HasText)
	{
		tag->InitialText = _tagStream->readString();
	}
}

void TagDecoder::cSMTextSettings( TagCSMTextSettings* tag )
{
}

void TagDecoder::showFrame( TagShowFrame* tag )
{
}

void TagDecoder::placeObject( TagPlaceObject* tag )
{
	assert(false);
}

void TagDecoder::placeObject2( TagPlaceObject2* tag )
{
	tag->flags1 = _tagStream->readU8();
	tag->Depth = _tagStream->readUI16();
	if (tag->hasCharID())
	{
		UI16 charId = _tagStream->readUI16();
		tag->Character = this->getDefineTag(charId);
		tag->putReference(tag->Character);
	}
	if (tag->hasMatrix())
	{
		tag->matrix = _tagStream->readMatrix();
	}
	if (tag->hasCxform())
	{
		tag->ColorTransform = _tagStream->readColorTransformWithAlpha();
	}
	if (tag->hasRatio())
	{
		tag->Ratio = _tagStream->readUI16();
	}
	if (tag->hasName())
	{
		tag->Name = _tagStream->readString();
	}
	if (tag->hasClipDepth())
	{
		tag->ClipDepth = _tagStream->readUI16();
	}
	if (tag->hasClipAction())
	{
		//TODO: decode ClipActions
		assert(false);
	}
}

void TagDecoder::placeObject3( TagPlaceObject3* tag )
{
	//assert(false);
}

void TagDecoder::defineBitsJPEG2( TagDefineBitsJPEG2* tag )
{
	tag->characterID = _tagStream->readUI16();
	_tagStream->readByteArray(&tag->imageData, _tagStream->getRemain());
}

void TagDecoder::defineBitsJPEG3( TagDefineBitsJPEG3* tag )
{
	tag->characterID = _tagStream->readUI16();
	
	UI32 AlphaDataOffset = _tagStream->readUI32();
	_tagStream->readByteArray(&tag->imageData, AlphaDataOffset);
	_tagStream->readByteArray(&tag->alphaData, _tagStream->getRemain());
}

void TagDecoder::defineBinaryData( TagDefineBinaryData* tag )
{
	tag->characterID = _tagStream->readUI16();
	_tagStream->readUI32();
	_tagStream->readByteArray(&tag->binary, _tagStream->getRemain());
}

void TagDecoder::productInfo( TagProductInfo* tag )
{
}

void TagDecoder::removeObject(TagRemoveObject* tag)
{
}

void TagDecoder::removeObject2(TagRemoveObject2* tag)
{
}

void TagDecoder::defineBitsLossless( TagDefineBitsLossless* tag )
{
}

void TagDecoder::defineBitsLossless2( TagDefineBitsLossless2* tag )
{
	tag->characterID = _tagStream->readUI16();
	tag->BitmapFormat = _tagStream->readUI8();
	tag->BitmapWidth = _tagStream->readUI16();
	tag->BitmapHeight = _tagStream->readUI16();
	if (tag->BitmapFormat == 3)
	{
		tag->BitmapColorTableSize = _tagStream->readUI8();
		//TODO:ALPHACOLORMAPDATA
		_tagStream->readByteArray(&tag->ZlibBitmapData, _tagStream->getRemain());
	}
	else if (tag->BitmapFormat == 4 || tag->BitmapFormat == 5)
	{
		//TODO:ALPHABITMAPDATA
		_tagStream->readByteArray(&tag->ZlibBitmapData, _tagStream->getRemain());
	}
}

void TagDecoder::defineSound( TagDefineSound* tag )
{
	tag->characterID = _tagStream->readUI16();
	_tagStream->readByteArray(&tag->soundData, _tagStream->getRemain());
}

void DefineSpriteDecoder::any(SWFTag* tag)
{
	_tag->tags.push_back(tag);
}