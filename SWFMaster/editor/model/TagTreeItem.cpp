#include "TagTreeItem.h"

TagTreeItem::TagTreeItem( SWFTag* tag )
	: _tag(tag)
{
	this->setKind(SWFTreeItemKind::Tag);
	this->setName(tag->getTagName());
	this->setUserData(tag);
}
