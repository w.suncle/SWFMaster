#include "swf/SWFTag.h"
#include "swf/SWFInputStream.h"
#include "swf/SWFOutputStream.h"
#include <QtZlib/zlib.h>

void TagDefineBitsJPEG2::setData( char* data, size_t len )
{
	SWFTag::setData(data, len);
}