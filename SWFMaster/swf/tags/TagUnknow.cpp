#include "swf/SWFTag.h"
#include "swf/SWFInputStream.h"

TagUnknow::TagUnknow( UI16 code )
{
	this->code = code;

	char names[1024] = {0};
	sprintf(names, "Unknow(%d)", code);

	this->_tagName = names;
}

std::string TagUnknow::getTagName() const
{
	return _tagName;
}