#include "ByteArray.h"
#include <stdlib.h>
#include <string>

ByteArray::ByteArray( char* data, size_t len )
	: data(data)
	, len(len)
	, allocLength(len)
{

}

ByteArray::ByteArray( size_t len )
	: len(len)
	, allocLength(len)
{
	data = (char*) malloc(len);
	memset(data, 0, len);
}

ByteArray::ByteArray()
	: data(nullptr)
	, len(0)
	, allocLength(0)
{

}

ByteArray::~ByteArray()
{
	if (data != nullptr)
		free(data);
}

void ByteArray::resize( size_t size )
{
	if (allocLength > size)
		return;
	
#define MALLOC_SIZE 1024 * 1024 //一次最少申请的内存数 1M
	if (size < MALLOC_SIZE)
		size = MALLOC_SIZE;

	char* oldData = data;
	char* newData = (char*)malloc(size);
	memset(newData, 0, size);
	if (len > 0)
	{
		size_t copySize = len;
		if (size < len)
			copySize = len;
		memcpy(newData, oldData, copySize);
		free(oldData);
	}
	this->data = newData;
	this->allocLength = size;
}

void ByteArray::copy( const char* data, size_t len, size_t offset /*= 0*/ )
{
	this->resize(offset + len);
	this->len = offset + len;
	memcpy(this->data + offset, data, len);
}

void ByteArray::clear()
{
	this->len = 0;
	if (allocLength > 0)
		memset(data, 0, allocLength);
}

