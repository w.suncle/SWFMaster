#include "SWFTagFactory.h"
#include "tags/TagDoABC.h"

SWFTag* SWFTagFactory::create( UI16 tagCode )
{
	SWFTag* tag = nullptr;
	auto itr = _creatorMap.find(tagCode);
	if (itr == _creatorMap.end())
		tag = new TagUnknow(tagCode);
	else
	{
		TagCreator creator = itr->second;
		tag = creator();
	}

	return tag;
}

SWFTagFactory::SWFTagFactory()
{
	this->registerTag<TagDoABC>(DoABC);	//82
	this->registerTag<TagDoABC>(72);	//72

	this->registerTag<TagProductInfo>(ProductInfo);
	this->registerTag<TagFrameLabel>(FrameLabel);
	this->registerTag<TagSetBackgroundColor>(SetBackgroundColor);
	this->registerTag<TagFileAttributes>(FileAttributes);
	this->registerTag<TagEnd>(End);
	this->registerTag<TagScriptLimits>(ScriptLimits);
	this->registerTag<TagSymbolClass>(SymbolClass);
	this->registerTag<TagImportAssets2>(ImportAssets2);
	this->registerTag<TagProtect>(Protect);
	this->registerTag<TagExportAssets>(ExportAssets);
	this->registerTag<TagEnableDebugger>(EnableDebugger);
	this->registerTag<TagEnableDebugger2>(EnableDebugger2);
	this->registerTag<TagSetTabIndex>(SetTabIndex);
	this->registerTag<TagDefineScalingGrid>(DefineScalingGrid);
	this->registerTag<TagDefineSceneAndFrameLabelData>(DefineSceneAndFrameLabelData);

	this->registerTag<TagDefineShape>(DefineShape);
	this->registerTag<TagDefineShape2>(DefineShape2);
	this->registerTag<TagDefineShape3>(DefineShape3);
	this->registerTag<TagDefineShape4>(DefineShape4);

	this->registerTag<TagDefineButton>(DefineButton);
	this->registerTag<TagDefineButton2>(DefineButton2);
	this->registerTag<TagDefineButtonCxform>(DefineButtonCxform);
	this->registerTag<TagDefineButtonSound>(DefineButtonSound);

	this->registerTag<TagDefineSprite>(DefineSprite);

	this->registerTag<TagDefineMorphShape>(DefineMorphShape);
	this->registerTag<TagDefineMorphShape2>(DefineMorphShape2);

	this->registerTag<TagDefineFont>(DefineFont);
	this->registerTag<TagDefineFont2>(DefineFont2);
	this->registerTag<TagDefineFont3>(DefineFont3);
	this->registerTag<TagDefineFont4>(DefineFont4);
	this->registerTag<TagDefineFontInfo>(DefineFontInfo);
	this->registerTag<TagDefineFontInfo2>(DefineFontInfo2);
	this->registerTag<TagDefineFontAlignZones>(DefineFontAlignZones);
	this->registerTag<TagDefineFontName>(DefineFontName);
	this->registerTag<TagDefineText>(DefineText);
	this->registerTag<TagDefineText2>(DefineText2);
	this->registerTag<TagDefineEditText>(DefineEditText);
	this->registerTag<TagCSMTextSettings>(CSMTextSettings);

	this->registerTag<TagShowFrame>(ShowFrame);
	this->registerTag<TagPlaceObject>(PlaceObject);
	this->registerTag<TagPlaceObject2>(PlaceObject2);
	this->registerTag<TagPlaceObject3>(PlaceObject3);
	this->registerTag<TagRemoveObject>(RemoveObject);
	this->registerTag<TagRemoveObject2>(RemoveObject2);

	this->registerTag<TagDefineBits>(DefineBits);
	this->registerTag<TagDefineBitsJPEG2>(DefineBitsJPEG2);
	this->registerTag<TagDefineBitsJPEG3>(DefineBitsJPEG3);
	this->registerTag<TagDefineBitsLossless>(DefineBitsLossless);
	this->registerTag<TagDefineBitsLossless2>(DefineBitsLossless2);

	this->registerTag<TagMetadata>(Metadata);
	this->registerTag<TagDefineBinaryData>(DefineBinaryData);

	this->registerTag<TagDefineSound>(DefineSound);
}
