#include "../SWFTag.h"

TagSetBackgroundColor::TagSetBackgroundColor()
	: SWFTag(SetBackgroundColor)
{

}

void TagSetBackgroundColor::setData(char* data, size_t len)
{
	SWFTag::setData(data, len);
}