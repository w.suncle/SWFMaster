#include "TagBuilder.h"
#include "../SWFTag.h"

SWFTag* PlaceObjectBuilder::build()
{
	TagPlaceObject2* placeObject2 = new TagPlaceObject2();
	placeObject2->code = PlaceObject2;
	placeObject2->putReference(_tag);
	placeObject2->setCharacter(_tag);

	return placeObject2;
}
