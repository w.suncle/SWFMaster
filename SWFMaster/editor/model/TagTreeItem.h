#ifndef TagTreeItem_h__
#define TagTreeItem_h__

#include "SWFTreeModel.h"
#include "swf/SWFTag.h"

class TagTreeItem
	: public SWFTreeItem
{
private:
	SWFTag* _tag;
public:
	TagTreeItem(SWFTag*);
	inline SWFTag* getTag() const { return _tag; }
};
#endif // TagTreeItem_h__
