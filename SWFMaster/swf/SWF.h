#ifndef __SWF_H__
#define __SWF_H__

#include "SWFTag.h"
#include "SWFTypes.h"
#include <vector>
#include <string>

class TagDoABC;
class SWFFrame;

class SWF
{
public:
	UI8 signature[4];
	UI8 version;
	UI16 frameRate;
	UI16 width;
	UI16 height;
public:
	SWFHeader header;
	TagFileAttributes* fileAttributes;
	TagEnableDebugger* enableDebugger;
	TagEnableDebugger2* enableDebugger2;
	TagMetadata* metadata;
	TagScriptLimits* scriptLimits;
	TagSetBackgroundColor* setBackgroundColor;
	TagDefineSceneAndFrameLabelData* defineSceneAndFrameLabelData;
	TagProductInfo* productInfo;
	TagProtect* protect;

	std::vector<SWFTag*> tags;
	std::vector<SWFFrame*> frames;

	SWF();
	~SWF();
public:
	void getImageTags( std::vector<SWFTag*>& output) const;
	void getShapeTags( std::vector<SWFTag*>& output) const;
	void getSpriteTags( std::vector<SWFTag*>& output) const;
	void getFontTags( std::vector<SWFTag*>& output) const;
	void getTextTags( std::vector<SWFTag*>& output) const;
	void getButtonTags( std::vector<SWFTag*>& output) const;
	void getOtherTags( std::vector<SWFTag*>& output) const;
};

class SWFFrame
{
public:
	SWFFrame();

	TagFrameLabel* frameLabel;
	TagSymbolClass* symbolClass;
	std::vector<DefineTag*> exportDefs;
	std::vector<TagDoABC*> doABCs;
	std::vector<TagImportAssets2*> imports;
	std::vector<TagDefineFont*> fonts;
	std::vector<SWFTag*> controlTags;

	void getAllDefineTags(std::vector<DefineTag*>& tags);
};

#endif // __SWF_H__
